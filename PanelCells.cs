﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BagFinder
{
    public partial class PanelCells : UserControl
    {
        #region Variables
        private int _iHorizDistanceCell = 5;
        private int _iVertDistanceCell = 5;
        private Size _SizeCell = new Size(50,50);
        public int _PanelCountCellsV = 7;
        public int _PanelCountCellsH = 7;
        public Cell[][] _cellsControls;
        #endregion
        #region Properties
        private int HorizDistance
        {
            get { return _iHorizDistanceCell; }
            set { _iHorizDistanceCell = value; }
        }
        private int VertDistance
        {
            get { return _iVertDistanceCell; }
            set { _iVertDistanceCell = value; }
        }
        public int CountCellsH
        {
            get { return _PanelCountCellsH; }
            set { _PanelCountCellsH = value; }
        }
        public int CountCellsV
        {
            get { return _PanelCountCellsV; }
            set { _PanelCountCellsV = value; }
        }
        public Cell[][] CellControls
        {
            get { return _cellsControls; }
            set { _cellsControls = value; }
        }
        #endregion


        public PanelCells()
        {
            InitializeComponent();
        }

        private void PanelCells_Load(object sender, EventArgs e)
        {
            CreateCellsByCount();
        }

        public void AutoSizePanelCells(int countV, int countH)
        {
            this.Width = countH * (_SizeCell.Width + HorizDistance) + HorizDistance * 2;
            this.Height = countV * (_SizeCell.Height + VertDistance) + VertDistance * 2;
        }


        private void CreateCellsBySize()
        {
            if (this.Width < _SizeCell.Width + 5 || this.Height < _SizeCell.Height + 5) return; // Exit

            int countH = this.Width / (_SizeCell.Width + 5);
            int countV = this.Height / (_SizeCell.Height + 5);
            CellControls = new Cell[countV][];
            for (int c = 0; c < countV; c++) CellControls[c] = new Cell[countH];
            int X, Y;
            int iBorderDist = HorizDistance;    // Up, Left 
            for (int i = 0; i <countV; i++)
                for (int j = 0; j < countH; j++)
                {
                    X = j * (_SizeCell.Width + HorizDistance) + iBorderDist;
                    Y = i * (_SizeCell.Height + VertDistance) + iBorderDist;
                    Cell cell = new Cell();
                    cell.Location = new Point(X,Y);
                    this.Controls.Add(cell);
                    CellControls[i][j] = cell;
                }
            //AutoSizePanelCells(countV, countH);
        }

        private void CreateCellsByCount()
        {
            //if (this.Width < _SizeCell.Width + 5 || this.Height < _SizeCell.Height + 5) return; // Exit

            int countH = CountCellsH;     //this.Width / (_SizeCell.Width + 5);
            int countV = CountCellsV;     // this.Height / (_SizeCell.Height + 5);
            CellControls = new Cell[countV][];
            for (int c = 0; c < countV; c++) CellControls[c] = new Cell[countH];
            int X, Y;
            int iBorderDist = HorizDistance;    // Up, Left 
            for (int i = 0; i < countV; i++)
                for (int j = 0; j < countH; j++)
                {
                    X = j * (_SizeCell.Width + HorizDistance) + iBorderDist;
                    Y = i * (_SizeCell.Height + VertDistance) + iBorderDist;
                    Cell cell = new Cell();
                    cell.Location = new Point(X, Y);
                    this.Controls.Add(cell);
                    CellControls[i][j] = cell;
                }
            AutoSizePanelCells(countV, countH);
        }
    }
}
