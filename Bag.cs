﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace BagFinder
{
    public class Bag
    {
        #region Variables

        private bool _bMarkChecked = false;             // Метка сигнализирующая о том что данная переменная был проверена
        private LineOnBag _lobWidth = null;             // ширина мешка
        private LineOnBag _lobLength = null;            // длина мешка

        private Point _pA = new Point(0, 0);
        private Point _pB = new Point(0, 0);
        private Point _pC = new Point(0, 0);
        private Point _pD = new Point(0, 0);

        private double _dArea = 0;                      // площадь
        private Point _pCenter = new Point(0, 0);       // центр фигуры
        private double _bAngle = 0;                     // угол относительно камеры, направление параллельно длинной стороне 

        // Static Parameters:
        private static double _divideWbyL = 37d / 60d;  // Отношение  ширины мешка к длине
        private static double _maxDivideError = 0.02d;  // Максимальное допустимое отклонение от заданного отношения ширины мешка к длине




        #region Property

        /// <summary>
        /// Отметка показывает что
        /// </summary>
        public bool IsMark
        {
            get { return _bMarkChecked; }
            set { _bMarkChecked = value; }
        }
        public LineOnBag LineOnBagWidth
        {
            get { return _lobWidth; }
            set { _lobWidth = value; }
        }
        public LineOnBag LineOnBagLength
        {
            get { return _lobLength; }
            set { _lobLength = value; }
        }

        public Point[] GetPoints
        {
            get
            {
                return new Point[] { _pA, _pB, _pC, _pD };
            }
        }
        public Point[] SetPoints
        {
            set
            {
                _pA = value[0];
                _pB = value[1];
                _pC = value[2];
                PartialInitialization();
            }
        }

        public Point SetA
        {
            set
            {
                _pA = value;
            }
        }
        public Point SetB
        {
            set
            {
                _pB = value;
            }
        }
        public Point SetC
        {
            set
            {
                _pC = value;
            }
        }


        public double BagArea
        {
            get { return _dArea; }
            set { _dArea = value; }
        }
        public double BagAngle
        {
            get { return _bAngle; }
            set { _bAngle = value; }
        }

        public static double DivideWbyL
        {
            get { return _divideWbyL; }
            set { _divideWbyL = value; }
        }
        public static double MaxDivideError
        {
            get { return _maxDivideError; }
            set { _maxDivideError = value; }
        }


        ///<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        #endregion

        #region CustPropery
        public Point GetBagCenter
        {
            get
            {
                return new Point((_pA.X + _pC.X) / 2, (_pA.Y + _pC.Y) / 2);
            }
        }
        public double GetWidth
        {
            get
            {
                double AB, BC;
                AB = Geometry.GetDistance(_pA, _pB);
                BC = Geometry.GetDistance(_pB, _pC);
                return AB > BC ? BC : AB;
            }
        }
        public double GetLength
        {
            get
            {
                double AB, BC;
                AB = Geometry.GetDistance(_pA, _pB);
                BC = Geometry.GetDistance(_pB, _pC);
                return AB > BC ? AB : BC;
            }
        }

        public int GetIndexWidthPoint
        {
            get
            {   // A B C D
                return Geometry.GetDistance(_pA, _pB) > Geometry.GetDistance(_pB, _pC) ? 2 : 0;
            }
        }
        public int GetIndexLengthPoint
        {
            get
            {   // A B C D
                return Geometry.GetDistance(_pA, _pB) > Geometry.GetDistance(_pB, _pC) ? 0 : 2;
            }
        }
        #endregion
        ///<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        #endregion



        #region Operator

        /// <summary>
        /// Сравнение площадей фигур
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
        public static bool operator >(Bag c1, Bag c2)
        {
            c1.FindArea();
            c2.FindArea();
            return c1._dArea > c2._dArea;
        }

        /// <summary>
        /// Сравнение площадей фигур
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
        public static bool operator <(Bag c1, Bag c2)
        {
            c1.FindArea();
            c2.FindArea();
            return c1._dArea < c2._dArea;
        }

        ///<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        #endregion



        #region FindVariables

        /// <summary>
        /// Поиск площади по имеющимся точками
        /// </summary>
        private void FindArea()
        {
            double BA = Math.Sqrt((_pA.X - _pB.X) * (_pA.X - _pB.X) + (_pA.Y - _pB.Y) * (_pA.Y - _pB.Y));
            double BC = Math.Sqrt((_pC.X - _pB.X) * (_pC.X - _pB.X) + (_pC.Y - _pB.Y) * (_pC.Y - _pB.Y));
            BagArea = BA * BC;
        }

        /// <summary>
        /// Поиск угла под которым лежит мешок. 90 градусов == вертикально расположенному мешку
        /// </summary>
        private void FindAngle()
        {
            BagAngle = Geometry.GetAngleBag(_pA, _pB);
        }

        /// <summary>
        /// Определение точек A,B,C
        /// </summary>
        private void FindTrangleABC()
        {
            if (LineOnBagWidth.A == LineOnBagLength.A)
            {
                _pA = LineOnBagWidth.B;
                _pB = LineOnBagLength.A;
                _pC = LineOnBagLength.B;
            }
            if (LineOnBagWidth.B == LineOnBagLength.A)
            {
                _pA = LineOnBagWidth.A;
                _pB = LineOnBagLength.A;
                _pC = LineOnBagLength.B;
            }


            if (LineOnBagWidth.A == LineOnBagLength.B)
            {
                _pA = LineOnBagWidth.B;
                _pB = LineOnBagLength.B;
                _pC = LineOnBagLength.A;
            }
            if (LineOnBagWidth.B == LineOnBagLength.B)
            {
                _pA = LineOnBagWidth.A;
                _pB = LineOnBagLength.B;
                _pC = LineOnBagLength.A;
            }
        }

        /// <summary>
        /// Поиск точки D по трем другим точкам
        /// </summary>
        private void FindPointD()
        {
            Point BC = new Point(_pC.X - _pB.X, _pC.Y - _pB.Y);
            _pD = new Point(_pA.X + BC.X, _pA.Y + BC.Y);
        }

        /// <summary>
        /// Поиск Пременных и их инициализация в класс
        /// </summary>
        public void FullInitialization()
        {
            FindTrangleABC();
            FindPointD();
            FindArea();
            FindAngle();
        }

        /// <summary>
        /// Поиск Переменных и их инициализация в класс. Точки a,b,c заданны вместо LineOnBag
        /// </summary>
        public void PartialInitialization()
        {
            FindPointD();
            FindArea();
            FindAngle();
        }

        ///<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        #endregion


        #region Private Methods

        /// <summary>
        /// Задает точки мешка и вызывает частичную инициализацию
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        private void SetBagByThreePoints(Point a, Point b, Point c)
        {
            _pA = a;
            _pB = b;
            _pC = c;
            PartialInitialization();
        }

        /// <summary>
        /// Сравнивает расстояние двух отрезок. Чем меньше процент тем меньше допустимая погрешность.
        /// </summary>
        /// <param name="segment1">любой отрезок</param>
        /// <param name="segment2">любой отрезок</param>
        /// <param name="percent">Допустимая погрешность в процентах от 0 до 100, [1;99]</param>
        /// <returns>Значение Истина достигается когда оба сегмента равны или находятся в пределах погрешности указанной в процентах</returns>
        private static bool IsTwoSegmentsEqualWithError(double segment1, double segment2)
        {
            double dPercent = DataSetting.PercentageOfMistakes;
            if (dPercent >= 100 || dPercent <= 0) throw new Exception();

            double dError = segment1 < segment2 ? segment1 * (dPercent / 100) : segment2 * (dPercent / 100);

            if (Math.Abs(segment1 - segment2) <= dError) return true;
            else return false;
        }

        //private static Size GetSizeOfBigBag(Bag[] ArrayOfBag)
        //{
        //    if (ArrayOfBag == null || ArrayOfBag.Length == 0) return new Size(0,0);

        //    Size size = new Size(ArrayOfBag[0].LineOnBagWidth)
        //    foreach(Bag b in ArrayOfBag)
        //    {

        //    }
        //}


        ///<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        #endregion



        #region Methods for Array

        /// <summary>
        /// Добавить мешок в массив
        /// </summary>
        /// <param name="bag">новый мешок</param>
        public static Bag[] AddBag(Bag bag, Bag[] ArrayOfBag)
        {
            if (ArrayOfBag == null || ArrayOfBag.Length == 0) { ArrayOfBag = new Bag[] { bag }; return ArrayOfBag; }

            Bag[] tmpArr = new Bag[ArrayOfBag.Length + 1];
            for (int i = 0; i < ArrayOfBag.Length; i++)
                tmpArr[i] = ArrayOfBag[i];
            tmpArr[tmpArr.Length - 1] = bag;
            return tmpArr;
        }

        /// <summary>
        /// Удалить первый мешок подходящий под описание.
        /// </summary>
        /// <param name="bag">мешок для удаления</param>
        public static Bag[] DeleteBag(Bag bag, Bag[] ArrayOfBag)
        {
            if (ArrayOfBag == null || ArrayOfBag.Length == 0) return ArrayOfBag;
            else if (ArrayOfBag.Length == 1) ArrayOfBag = null;

            int indexForDelete = -1;
            for (int i = 0; i < ArrayOfBag.Length; i++)
                if (ArrayOfBag[i] == bag)
                {
                    indexForDelete = i;
                    break;
                }

            if (indexForDelete < 0 || indexForDelete >= ArrayOfBag.Length) return ArrayOfBag;


            Bag[] tmpArr = new Bag[ArrayOfBag.Length - 1];
            for (int indexNew = 0, indexOld = 0; indexNew < ArrayOfBag.Length; indexOld++)
                if (indexOld != indexForDelete)
                {
                    tmpArr[indexNew] = ArrayOfBag[indexOld];
                    indexNew++;
                }
            return tmpArr;
        }

        /// <summary>
        /// Удалить мешок по индексу.
        /// </summary>
        /// <param name="bag">мешок для удаления</param>
        public static Bag[] DeleteBag(int index, Bag[] ArrayOfBag)
        {
            if (ArrayOfBag == null || ArrayOfBag.Length == 0) return ArrayOfBag;
            if (index < 0 || index >= ArrayOfBag.Length) return ArrayOfBag;
            if (ArrayOfBag.Length == 1) return null;


            Bag[] tmpArr = new Bag[ArrayOfBag.Length - 1];
            for (int indexNew = 0, indexOld = 0; indexOld < ArrayOfBag.Length; indexOld++)
            {
                if (indexOld == index) continue;
                tmpArr[indexNew] = ArrayOfBag[indexOld];
                indexNew++;
            }

            return tmpArr;
        }

        /// <summary>
        /// Заменить мешок по индексу.
        /// </summary>
        /// <param name="bag">мешок для замены</param>
        public static Bag[] ReplaceBag(Bag bag, int index, Bag[] ArrayOfBag)
        {
            if (ArrayOfBag == null || ArrayOfBag.Length == 0) return ArrayOfBag;
            if (index < 0 || index >= ArrayOfBag.Length) return ArrayOfBag;


            Bag[] tmpArr = new Bag[ArrayOfBag.Length];
            for (int i = 0; i < ArrayOfBag.Length; i++)
                if (i != index) tmpArr[i] = ArrayOfBag[i];
                else tmpArr[i] = bag;

            return tmpArr;
        }


        ///<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        #endregion



        #region ClosedBags

        /// <summary>
        /// Проверка мешков. Сравнивается расстояние между двумя центрами с шириной большого мешка
        /// </summary2345``12>
        /// <param name="bag1"></param>
        /// <param name="bag2"></param>
        /// <returns>Истина если дубликат мешка</returns>
        private static bool IsBagToClose(Bag bag1, Bag bag2)
        {
            Point center1, center2;
            double width, dist;

            center1 = bag1.GetBagCenter;
            center2 = bag2.GetBagCenter;

            dist = Geometry.GetDistance(center1, center2);

            if (bag1.BagArea > bag2.BagArea)
            {
                if (Geometry.GetDistance(bag1.GetPoints[0], bag1.GetPoints[1]) < Geometry.GetDistance(bag1.GetPoints[1], bag1.GetPoints[2]))
                    width = Geometry.GetDistance(bag1.GetPoints[0], bag1.GetPoints[1]);
                else width = Geometry.GetDistance(bag1.GetPoints[1], bag1.GetPoints[2]);
            }
            else
            {
                if (Geometry.GetDistance(bag2.GetPoints[0], bag2.GetPoints[1]) < Geometry.GetDistance(bag2.GetPoints[1], bag2.GetPoints[2]))
                    width = Geometry.GetDistance(bag2.GetPoints[0], bag2.GetPoints[1]);
                else width = Geometry.GetDistance(bag2.GetPoints[1], bag2.GetPoints[2]);
            }

            if (dist > width) return false;                                                         // Расстояние между мешками больше ширины мешка
            else return IsTwoSegmentsEqualWithError(dist, width);             // Расстояние между мешками должно быть в допустимой погрешности: PercentageOfMistakes

            //else if (dist < width && Math.Abs(width - dist) < width / 20) return false;             // Расстояние между мешками в 5% ой допустимой погрешности
            //else return true;                                                                       // Расстояние между мешками слишком близко, следовательно это тот же мешок
        }

        /// <summary>
        /// Проверяет имеется ли дубликат мешка в массиве. За счет сравнения расстояния центров мешков
        /// </summary>
        /// <param name="bag">Проверяемый мешок</param>
        /// <param name="ArrayOfBag">Массив мешков</param>
        /// <returns>Возвращает индекс дубликата мешка, если индекс -1 то мешок уникален</returns>
        public static int FindToCloseBags(Bag bag, Bag[] ArrayOfBag)
        {
            if (ArrayOfBag == null || ArrayOfBag.Length == 0) return -1;

            for (int i = 0; i < ArrayOfBag.Length; i++)
                if (IsBagToClose(ArrayOfBag[i], bag))
                    return i;

            return -1;
        }

        /// <summary>
        /// Проверяет расстояние между данным мешком и мешками в массиве
        /// </summary>
        /// <param name="bag">Мешок</param>
        /// <param name="ArrayOfBag">Массив</param>
        /// <returns>Возвращает Истину если найден дубликат</returns>
        public static bool IsBagTooCloseExist(Bag bag, Bag[] ArrayOfBag)
        {
            ClearToCloseBags(ArrayOfBag);
            int index = FindToCloseBags(bag, ArrayOfBag);
            if (index == -1) return false;
            else return true;
        }

        /// <summary>
        /// Удаляет из массива мешки находящиеся слишком близко
        /// </summary>
        /// <param name="ArrayOfBag">Массив элементы которого проверяются</param>
        public static void ClearToCloseBags(Bag[] ArrayOfBag)
        {
            if (ArrayOfBag == null || ArrayOfBag.Length == 0) return;

            for (int i = ArrayOfBag.Length - 1; i > 0; i--)
            {
                int index = FindToCloseBags(ArrayOfBag[i], ArrayOfBag);
                if (index == i || index == -1) continue;
                ArrayOfBag = DeleteBag(index, ArrayOfBag);
            }
        }

        ///<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        #endregion



        #region Unsorted Methods

        /// <summary>
        /// Проверка всех мешков на наличие линии близкой к одной из сторон. Мешок отмечается если линия является частью мешка.
        /// Отметка остается после достижения конца CounterFrames.
        /// Мешок не проверяется если есть отметка.
        /// </summary>
        /// <param name="lob">линия для сравнения</param>
        public static void CheckingLinesOnBag(LineOnBag lob, Bag[] ArrayOfBag)
        {
            foreach (Bag b in ArrayOfBag)
                if (CompareLineWithBag(lob, b, ArrayOfBag))
                {
                    b.IsMark = true;
                    return;
                }
        }


        /// <summary>
        /// Сброс всех меток на мешках в false 
        /// </summary>
        public static void ResetMarksOnBags(Bag[] ArrayOfBag)
        {
            foreach (Bag b in ArrayOfBag)
                b.IsMark = false;
        }

        public static bool CompareLineWithBag(LineOnBag lob, Bag bag, Bag[] ArrayOfBag)
        {
            // Если линия  лежит на мешке возвращает true
            return true;

        }

        /// <summary>
        /// Создать мешок п трём точкам
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="bag">Возвращаемый мешок</param>
        /// <returns>Возвращает Истину при успешном создании мешка</returns>
        public static bool CreateBag(Point a, Point b, Point c, out Bag bag, Sides sideH, Sides sideV)
        {
            bag = new Bag();
            double length = 0, width = 0;
            double dAngle = Geometry.GetAngle(a, b, c);
            if (Math.Abs(90 - dAngle) < 4)
            {
                if (Geometry.GetDistance(a, b) > Geometry.GetDistance(b, c))
                {
                    length = Geometry.GetDistance(a, b);
                    width = Geometry.GetDistance(b, c);
                }
                else
                {
                    width = Geometry.GetDistance(a, b);
                    length = Geometry.GetDistance(b, c);
                }

                if (Math.Abs(width / length - DivideWbyL) < MaxDivideError)
                {
                    // Расстояние сторон в пределах нормы
                    bag.SetBagByThreePoints(a, b, c);
                    return !bag.IsBagFalse(sideH, sideV);
                }
                else return false;
            }
            return false;
        }

        /// <summary>
        /// Удлиняет контуры коротких мешков до самого большого мешка
        /// </summary>
        /// <param name="ArrayOfBag">массив мешков</param>
        /// <returns>массив одинаковых мешков</returns>
        public static Bag[] BagsFit(Bag[] ArrayOfBag)
        {
            if (ArrayOfBag == null || ArrayOfBag.Length < 1)    // Если мешков меньше двух метод прерывается
                return ArrayOfBag;
            Bag bigBag = GetBigBag(ArrayOfBag);

            double width = bigBag.GetWidth;
            double length = bigBag.GetLength;

            for(int i=0; i <ArrayOfBag.Length;i++)
            {
                Point EndPoint, BeginPoint, Vector, newPointW, newPointL;
                PointF UnitVector;
                bool bNewWPoint = false;
                bool bNewLPoint = false;
                newPointW = new Point(-1, -1);
                newPointL = new Point(-1, -1);

                if ((width - ArrayOfBag[i].GetWidth) > width * DataSetting.PercentageOfMistakes / 100)  // погрешность состовляет 5%
                {
                    EndPoint = ArrayOfBag[i].GetPoints[ArrayOfBag[i].GetIndexWidthPoint];   // Конец вектора
                    BeginPoint = ArrayOfBag[i].GetPoints[1];                                  // Начало вектора
                    Vector = new Point(EndPoint.X - BeginPoint.X, EndPoint.Y - BeginPoint.Y);         // Вектор   WPoint - MPoint
                    newPointW = new Point((int)(BeginPoint.X + Vector.X * width / Geometry.GetDistance(EndPoint, BeginPoint)),
                        (int)(BeginPoint.Y + Vector.Y * width / Geometry.GetDistance(EndPoint, BeginPoint)));
                    bNewWPoint = true;
                }

                if ((length - ArrayOfBag[i].GetLength) > length * DataSetting.PercentageOfMistakes / 100)  // погрешность состовляет 5%
                {
                    EndPoint = ArrayOfBag[i].GetPoints[ArrayOfBag[i].GetIndexLengthPoint];   // Конец вектора
                    BeginPoint = ArrayOfBag[i].GetPoints[1];                                  // Начало вектора
                    Vector = new Point(EndPoint.X - BeginPoint.X, EndPoint.Y - BeginPoint.Y);         // Вектор   WPoint - MPoint
                    UnitVector = Geometry.GetUnitVector(Vector);
                    newPointL = new Point((int)(BeginPoint.X + Vector.X * length / Geometry.GetDistance(EndPoint, BeginPoint)),
                        (int)(BeginPoint.Y + Vector.Y * length / Geometry.GetDistance(EndPoint, BeginPoint)));
                    bNewLPoint = true;
                }

                if (bNewWPoint || bNewLPoint)
                {
                    Point p0 = bNewWPoint ? newPointW : ArrayOfBag[i].GetPoints[ArrayOfBag[i].GetIndexWidthPoint];
                    Point p1 = ArrayOfBag[i].GetPoints[1];
                    Point p2 = bNewLPoint ? newPointL : ArrayOfBag[i].GetPoints[ArrayOfBag[i].GetIndexLengthPoint];
                    Point p3 = new Point(0, 0);

                    ArrayOfBag[i].SetPoints = new Point[] { p0, p1, p2, p3 };
                }
            }

            return ArrayOfBag;
        }

        /// <summary>
        /// Если мешок не в области где она расположен, иначе говоря данный мешок состоит из сторон двух других мешков.
        /// Тогда возвращается Истина
        /// </summary>
        /// <param name="sideH">Слева или справа расположение линии</param>
        /// <param name="sideV">Сверху или снизу расположение линии</param>
        /// <returns>Ложь возвращается если мешок является настоящим</returns>
        private bool IsBagFalse(Sides sideH, Sides sideV)
        {
            // P0 == _pB; P1 ==_pD;
            Point P0 = this.GetPoints[1];
            Point P1 = this.GetPoints[3];

            bool bCheckX, bCheckY;
            // up down
            if (sideV == Sides.Up) bCheckY = P0.Y > P1.Y;
            else bCheckY = P0.Y < P1.Y;
            // left right
            if (sideH == Sides.Left) bCheckX = P0.X > P1.X;
            else bCheckX = P0.X < P1.X;

            if (bCheckX && bCheckY) return true;
            else return false;
        }

        /// <summary>
        /// Ряд проверок мешка. Сравнение ближайщих и объединение.
        /// </summary>
        /// <param name="bag"></param>
        /// <param name="ArrayOfBag"></param>
        /// <returns></returns>
        public static Bag[] CheckAndAddBag(Bag bag, Bag[] ArrayOfBag)
        {
            if (ArrayOfBag == null || ArrayOfBag.Length == 0)
            {
                ArrayOfBag = AddBag(bag, ArrayOfBag);
                return ArrayOfBag;
            }

            Bag bigBag = GetBigBag(ArrayOfBag);
            double dDistanceLimitOnPrecent = bigBag.GetWidth;// * 0.90d;
            for (int i = 0; i < ArrayOfBag.Length; i++)
            {
                if (IsNativeBag(bag, ArrayOfBag[i])) // два мешка расположены с одного угла
                {
                    Bag newBag = CombineBags(bag, ArrayOfBag[i], ArrayOfBag);
                    ArrayOfBag = DeleteBag(i, ArrayOfBag);
                    ArrayOfBag = AddBag(newBag, ArrayOfBag);
                    ArrayOfBag = RemoveDuplicateBags(ArrayOfBag);
                    return ArrayOfBag;
                }
                if (IsCentersBagsTooClose(bag, ArrayOfBag[i], dDistanceLimitOnPrecent) && bag.CompareArea(ArrayOfBag[i]))
                {
                    ArrayOfBag = DeleteBag(i, ArrayOfBag);
                    ArrayOfBag = AddBag(bag, ArrayOfBag);
                    ArrayOfBag = RemoveDuplicateBags(ArrayOfBag);
                    return ArrayOfBag;
                }
            }


            ArrayOfBag = AddBag(bag, ArrayOfBag);
            return ArrayOfBag;
        }

        /// <summary>
        /// Проверка что оба мешка лежат в одной точке с одного угла. Возможно лишь разная длина сторон
        /// </summary>
        /// <param name="b1">Мешок 1</param>
        /// <param name="b2">Мешок 2</param>
        /// <returns>Если мешки лежат на одном угле, результат Истина</returns>
        private static bool IsNativeBag(Bag b1, Bag b2)
        {
            // Расстояние между точками B двух разных мешков
            double bigWidth = b1.GetWidth > b2.GetWidth ? b1.GetWidth : b2.GetWidth;
            double dAllowedDist = bigWidth * (DataSetting.PercentageOfMistakes / 100);

            if (Geometry.GetDistance(b1.GetPoints[1], b2.GetPoints[1]) < dAllowedDist)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Объединение самых отдаленных точек мешков. И частичная инициализация.
        /// Точка пересечения двух линий определяется случайно.
        /// </summary>
        /// <param name="b1">Мешок 1</param>
        /// <param name="b2">Мешок 2</param>
        /// <returns>Результат сложения мешков</returns>
        private static Bag CombineBags(Bag b1, Bag b2, Bag[] ArrayOfBag)
        {
            double dist = Geometry.GetDistance(b1.GetPoints[0], b2.GetPoints[0]);
            int iIndexA = 0;                                                            // Точка А(индекс 0) мешка b2 Близко к точке A мешка b1

            if (dist > Geometry.GetDistance(b1.GetPoints[0], b2.GetPoints[2]))
                iIndexA = 2;                                                            // Точка C(индекс 2) мешка b2 Близко к точке A мешка b1

            int iIndexC = iIndexA == 0 ? 2 : 0;                                         // Точка C определяется в зависимости от точки А
            Bag bagCombined = b1;
            Point pB1PointA = Geometry.GetDistantPoint(b1.GetPoints[1], b1.GetPoints[0], b2.GetPoints[iIndexA]);
            Point pB2PointA = Geometry.GetDistantPoint(b2.GetPoints[1], b1.GetPoints[0], b2.GetPoints[iIndexA]);

            Point pB1PointC = Geometry.GetDistantPoint(b1.GetPoints[1], b1.GetPoints[2], b2.GetPoints[iIndexC]);
            Point pB2PointC = Geometry.GetDistantPoint(b2.GetPoints[1], b1.GetPoints[2], b2.GetPoints[iIndexC]);

            // Установка точки A
            if (Geometry.GetDistance(b1.GetPoints[1], pB1PointA) > Geometry.GetDistance(b2.GetPoints[1], pB2PointA))
            {
                bagCombined.SetB = b1.GetPoints[1];
                bagCombined.SetA = pB1PointA;
            }
            else
            {
                bagCombined.SetB = b2.GetPoints[1];
                bagCombined.SetA = pB2PointA;
            }
            // Установка точки B
            if (Geometry.GetDistance(b1.GetPoints[1], pB1PointC) > Geometry.GetDistance(b2.GetPoints[1], pB2PointC))
            {
                bagCombined.SetB = b1.GetPoints[1];
                bagCombined.SetC = pB1PointC;
            }
            else
            {
                bagCombined.SetB = b2.GetPoints[1];
                bagCombined.SetC = pB2PointC;
            }

            bagCombined.PartialInitialization();
            if (BagIsIntersect(bagCombined, ArrayOfBag))
                return b1;
            else
                return bagCombined;
        }

        private static bool BagIsIntersect(Bag b1, Bag[] ArrayOfBag)
        {
            foreach (Bag b in ArrayOfBag)
                if (LinesIsIntersect(b1, b))
                    return true;
            return false;
        }

        /// <summary>
        /// Отрезок пересекает один из мешков
        /// </summary>
        /// <param name="A">начало отрезка</param>
        /// <param name="B">конец отрезка</param>
        /// <param name="bag">массив мешков</param>
        /// <returns></returns>
        private static bool LineIsIntersect(Point A, Point B, Bag bag)
        {
            Point bagA = bag.GetPoints[0];
            Point bagB = bag.GetPoints[1];
            Point bagC = bag.GetPoints[2];
            Point bagD = bag.GetPoints[3];
            bool bIsIntersectAB = Geometry.IsIntersection(A, B, bagA, bagB);
            bool bIsIntersectBC = Geometry.IsIntersection(A, B, bagB, bagC);
            bool bIsIntersectCD = Geometry.IsIntersection(A, B, bagC, bagD);
            bool bIsIntersectDA = Geometry.IsIntersection(A, B, bagD, bagA);

            if (bIsIntersectAB || bIsIntersectBC || bIsIntersectCD || bIsIntersectDA) return true;
            return false;
        }

        private static bool LinesIsIntersect(Bag bag1, Bag bag2)
        {
            Point bagA = bag1.GetPoints[0];
            Point bagB = bag1.GetPoints[1];
            Point bagC = bag1.GetPoints[2];
            Point bagD = bag1.GetPoints[3];
            bool bIsIntersectAB = LineIsIntersect(bagA, bagB, bag2);
            bool bIsIntersectBC = LineIsIntersect(bagB, bagC, bag2);
            bool bIsIntersectCD = LineIsIntersect(bagC, bagD, bag2);
            bool bIsIntersectDA = LineIsIntersect(bagD, bagA, bag2);

            if (bIsIntersectAB || bIsIntersectBC || bIsIntersectCD || bIsIntersectDA) return true;
            return false;
        }

        /// <summary>
        /// Поиск самого большого мешка(по площади)
        /// </summary>
        /// <param name="ArrayOfBag">Массив мешков</param>
        /// <returns>Мешок с большей площадью</returns>
        public static Bag GetBigBag(Bag[] ArrayOfBag)
        {

            Bag bigBag = ArrayOfBag[0];
            for (int i = 0; i < ArrayOfBag.Length; i++)
                if (bigBag.BagArea < ArrayOfBag[i].BagArea)
                    bigBag = ArrayOfBag[i];
            return bigBag;
        }

        /// <summary>
        /// Сравнивает площади двух мешков. Если разница между мешками составляет меньше PercentageOfMistakes % возвращает истину. 
        /// Для сравнения берется площадь большего мешка.
        /// </summary>
        /// <param name="bag1"></param>
        /// <param name="bag2"></param>
        /// <returns>Ложь если разница площадей большая</returns>
        public static bool CompareBagWithArea(Bag bag1, Bag bag2)
        {
            double dPercent = DataSetting.PercentageOfMistakes;

            if (dPercent >= 100 || dPercent <= 0) throw new Exception();

            double dMaxArea, dMinArea;
            dMaxArea = bag1.BagArea > bag2.BagArea ? bag1.BagArea : bag2.BagArea;
            dMinArea = bag1.BagArea < bag2.BagArea ? bag1.BagArea : bag2.BagArea;

            if (bag1.BagArea == bag2.BagArea) return true;

            if (dMaxArea - dMinArea <= dMaxArea * (dPercent / 100))
                return true;
            else return false;
        }

        /// <summary>
        /// Проверяет, больше ли площадь мешка по сравнению заданным мешком в аргументе.
        /// </summary>
        /// <param name="bag">второй мешок</param>
        /// <returns></returns>
        private bool CompareArea(Bag bag)
        {
            if (this.BagArea > bag.BagArea) return true;
            else return false;
        }

        static public Bag[] RemoveDuplicateBags(Bag[] ArrayOfBag)
        {
            List<int> BagsForDelete = new List<int>();
            for (int i = 0; i < ArrayOfBag.Length;i++)
                for (int j = 0; j < ArrayOfBag.Length; j++)
                {
                    if (i == j) continue;
                    if (ComparePoints(ArrayOfBag[i], ArrayOfBag[j]))
                        if (ArrayOfBag[i].CompareArea(ArrayOfBag[j]))
                            BagsForDelete.Add(j);
                        else
                            BagsForDelete.Add(i);
                }

            BagsForDelete.Reverse();                        // Расставить числа в порядке убывания
            int lastIndex = -1;                             // Если индекс повторится то он проигнорируется
            foreach (int index in BagsForDelete)
            {
                if (index == lastIndex) continue;
                lastIndex = index;
                ArrayOfBag = DeleteBag(index, ArrayOfBag);
            }

            return ArrayOfBag;
        }

        /// <summary>
        /// Проверяет находится ли мешок внутри другого мешка
        /// </summary>
        /// <param name="bag"></param>
        /// <returns></returns>
        static private bool ComparePoints(Bag b1, Bag b2)
        {
            if (IsPointOnCircle(b1.GetBagCenter, b1.GetWidth, b2.GetBagCenter) || IsPointOnCircle(b2.GetBagCenter, b2.GetWidth, b1.GetBagCenter))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Проверка положение точки относительно окружности.
        /// </summary>
        /// <param name="circle">Центр окружности</param>
        /// <param name="radius">Радиус</param>
        /// <param name="point">Проверяемая точка</param>
        /// <returns>Если точка внутри круга, возвращается истина</returns>
        static private bool IsPointOnCircle(Point circle, double radius, Point point)
        {
            double temp = (point.X - circle.X) * (point.X - circle.X) + (point.Y - circle.Y) * (point.Y - circle.Y);
            if (temp < radius * radius) return true;    // <=
            return false;
        }

        /// <summary>
        /// Сравнивает расстояние между центрами мешков.
        /// </summary>
        /// <param name="b1">мешок 1</param>
        /// <param name="b2">мешок 2</param>
        /// <param name="dist">максимальное расстояние между мешками</param>
        /// <returns>Если расстояние меньше чем заданное в аргументе, то возвращает true.</returns>
        private static bool IsCentersBagsTooClose(Bag b1, Bag b2, double dist)
        {
            Point pCenter1 = Geometry.GetMidlePoint(b1.GetPoints[2], b1.GetPoints[0]);
            Point pCenter2 = Geometry.GetMidlePoint(b2.GetPoints[2], b2.GetPoints[0]);

            double dDistanceC1C2 = Geometry.GetDistance(pCenter1, pCenter2);
            if (dDistanceC1C2 < dist) return true;
            else return false;
        }

        /// <summary>
        /// Сравнивает две начальные стороны обоих мешков. Проверяет что оба мешка накладываются друг на друга
        /// </summary>
        /// <param name="b1">Мешок 1</param>
        /// <param name="b2">Мешок 2</param>
        /// <returns>Если стороны мешков попарно параллельны, возвращает true.</returns>
        private bool IsParallelBags(Bag b1, Bag b2)
        {
            LineOnBag lobNew1, lobNew2;
            bool bCombined1, bCombined2;

            bCombined1 = LineOnBag.IsSuccessfullyCombined(b1.LineOnBagWidth, b2.LineOnBagWidth,
                DataSetting.MaxSamplingErrorForLOB, DataSetting.MaxSpacingErrorForLOB, out lobNew1);
            bCombined2 = LineOnBag.IsSuccessfullyCombined(b1.LineOnBagLength, b2.LineOnBagLength,
                DataSetting.MaxSamplingErrorForLOB, DataSetting.MaxSpacingErrorForLOB, out lobNew2);
            if (bCombined1 && bCombined2) return true;

            bCombined1 = LineOnBag.IsSuccessfullyCombined(b1.LineOnBagWidth, b2.LineOnBagLength,
                DataSetting.MaxSamplingErrorForLOB, DataSetting.MaxSpacingErrorForLOB, out lobNew1);
            bCombined2 = LineOnBag.IsSuccessfullyCombined(b1.LineOnBagLength, b2.LineOnBagWidth,
                DataSetting.MaxSamplingErrorForLOB, DataSetting.MaxSpacingErrorForLOB, out lobNew2);
            if (bCombined1 && bCombined2) return true;

            return false;
        }

        public static void CheckBags()
        {
            /// Объединить IsBagDuplicate и  CompareBagsWithArea
        }

        public static void CompareBagsWithArea(Bag bag, Bag[] ArrayOfBag)
        {
            for (int i = ArrayOfBag.Length - 1; i >= 0; i--)
            {
                bool bIdenticalBags = CompareBagWithArea(bag, ArrayOfBag[i]);  // Если площади почти равны то истина
                if (bIdenticalBags && bag.CompareArea(ArrayOfBag[i]))
                {
                    ArrayOfBag = DeleteBag(i, ArrayOfBag);
                    ArrayOfBag = AddBag(bag, ArrayOfBag);
                    return;
                }
            }
            //Удалять с маленькой площадью мешки.
        }

        ///<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        #endregion


    }
}