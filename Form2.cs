﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.MemoryMappedFiles;
using System.IO;

namespace BagFinder
{
    public partial class Form2 : Form
    {
        #region Variables
        private Size _SizeBI = new Size(50, 50);

        #region Properties

        #endregion
        #endregion

        public Form2()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(Form2_FormClosed);
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void Form2_FormClosed(object sender, FormClosingEventArgs e)
        {
            tm.Stop();
        }

        #region Form for emulation connection with camera

        string path = "c:\\SickBitmap\\";         //"c:\\SickBitmapPallet2\\" "c:\\SickBitmap\\"      //SickBitmapPallet
        string name = "sick_";
        int imageNumb = 0;
        string format = ".bmp";
        PictureBox pictureBox = null;
        Timer tm = new Timer();

        public void StartEmulation(PictureBox pbox)
        {
            pictureBox = pbox;
            tm.Interval = 33;
            tm.Tick += new EventHandler(EmulationSick);
            tm.Start();
            //this.Invoke(new Action(() => EmulationSick(pbox)));
        }
        private void EmulationSick(object sender, EventArgs e)
        {
            string path1 = path + name + imageNumb + format; imageNumb++;
            //string path2 = "c:\\SickBitmap\\sick_" + imageNumb + ".bmp";
            if (!System.IO.File.Exists(path1))
            {
                imageNumb = 0;
                return;
            }
            Image image = Image.FromFile(path1);
            WriteAsync(new Bitmap(image));
            pictureBox.Image = image; 
        }

        MemoryMappedFile memoryServer;
        private const string memoryName = "MemoryServer";
        private int imageNumber = 0;
        public void WriteAsync(Bitmap _bitmap)
        {
            // isRun предотвращает одновременную запись в ту же память
            // serverControl != clientControl ждет окончание чтения клиента перед новой записью
            // serverControl == clientControl в том случае если сообщение было прочтено клиентом
            //if (isRun) return;
            //isRun = true;

            byte[] array_bitmap = BitmapToByte(_bitmap);//BitmapToByte(_bitmap);    // Заголовок состовляет 54 байта
            int size = array_bitmap.Length;

            memoryServer = MemoryMappedFile.CreateOrOpen(memoryName, size + 8);

            imageNumber++;
            if (imageNumber > 1000) imageNumber = 0;

            using (MemoryMappedViewAccessor writer = memoryServer.CreateViewAccessor(0, size + 8))
            {
                //serverControl = ~serverControl * 0x01;
                writer.Write(0, imageNumber);               // Number
                writer.Write(4, size);                      // size of array
                writer.WriteArray<byte>(8, array_bitmap, 0, array_bitmap.Length);   // array
            }
            //isRun = false;
        }
        private static byte[] BitmapToByte(Bitmap bm)
        {
            using (var stream = new MemoryStream())
            {
                bm.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
                return stream.ToArray();
            }
        }
        #endregion
    }
}
