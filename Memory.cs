﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.MemoryMappedFiles;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.IO;

using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

namespace BagFinder
{
    class Memory : MemoryManagerClient
    {
        public enum ReadingMode
        {
            OriginalMode = 0,
            Cleaner1 = 1,
            Cleaner2 = 2,
            TwoFrame = 3,
            ThreeFrame = 4
        }

        //public MemoryManagerClient _StreamController = new MemoryManagerClient();

        private Bitmap _lastImage = null;
        private Bitmap _imageToShow = null;
        private Bitmap _bitmap1 = null;
        private Bitmap _bitmap2 = null;
        private Bitmap _bitmap3 = null;
        private Bitmap _midleResult;
        private int _iModeCounter = 0;
        private bool _itReading = false;
        private bool _itNew = false;
        private string _sException = "Exception: ";
        private int _iDelayRead = 10;
        private Form1 _Form1Link;
        private ReadingMode _readingMode = ReadingMode.OriginalMode;


        #region Property
        private Bitmap Bitmap1
        {
            set { _bitmap1 = value; }
            get { return _bitmap1; }
        }
        private Bitmap Bitmap2
        {
            set { _bitmap2 = value; }
            get { return _bitmap2; }
        }
        private Bitmap Bitmap3
        {
            set { _bitmap3 = value; }
            get { return _bitmap3; }
        }
        private Bitmap MidleResult
        {
            set { _midleResult = value; }
            get { return _midleResult; }
        }
        private int ModeCounter
        {
            set { _iModeCounter = value; }
            get { return _iModeCounter; }
        }
        private Form1 LinkForm1
        {
            set { _Form1Link = value; }
            get { return _Form1Link; }
        }

        public int DelayRead
        {
            set { _iDelayRead = value; }
            get { return _iDelayRead; }
        }
        public ReadingMode ReadMode
        {
            set { _readingMode = value; }
            get { return _readingMode; }
        }
        public Bitmap LastImage
        {
            set { _lastImage = value; }
            get { return _lastImage; }
        }
        public Bitmap ImageToShow
        {
            set { _imageToShow = value; }
            get { return _imageToShow; }
        }
        public bool ItReading
        {
            set { _itReading = value; }
            get { return _itReading; }
        }
        public bool ItNew
        {
            set { _itNew = value; }
            get { return _itNew; }
        }
        public string StrException
        {
            set { _sException = value; }
            get { return _sException; }
        }
        #endregion



        public void Start(Form1 f1)
        {
            LinkForm1 = f1;
            ReadMemoryTask();
        }
        public void ReadMemoryTask()
        {   // Recursion
            ItNew = true;
            Task t = Task.Run(() =>
            {
                while (true)
                {
                    ReadMemory();
                }
                                //itReading = ReadImageFromMemory();
            });
            //t.Wait(DelayRead);
        }
        public void ReadMemory()
        {
            Bitmap bmp = Read();
            if (bmp != null)
            {
                switch(ReadMode)
                {
                    case ReadingMode.OriginalMode:
                        OriginalMode(bmp);
                        break;
                    case ReadingMode.Cleaner1:
                        CleanerMode1(bmp);
                        break;
                    case ReadingMode.Cleaner2:
                        CleanerMode2(bmp);
                        break;
                    case ReadingMode.TwoFrame:
                        TwoFrameMode(bmp);
                        break;
                    case ReadingMode.ThreeFrame:
                        ThridMode(bmp);
                        break;
                    default:
                        MessageBox.Show("ERROR! ReadMemory() -> ReadingMode.default!");
                        ItReading = false;
                        break;
                }
            }
            else ItReading = false;
            //ReadMemoryTask();
        }
        //BackUp        ReadMemory
        //private void ReadMemory()
        //{
        //    Bitmap bmp = StreamController.Read();
        //    if (bmp != null)
        //    {
        //        LastImage = ImageExplorer.NoiseCleaner(bmp, GetNumbRadioButton());
        //        ItReading = true;
        //        ItNew = true;
        //    }
        //    else ItReading = false;
        //    ReadImage();
        //}
        private void OriginalMode(Bitmap bmp)
        {
            LastImage = bmp;
            EndOfMode();
        }
        private void CleanerMode1(Bitmap bmp)
        {
            LastImage = ImageExplorer.NoiseCleaner(bmp, 1);
            EndOfMode();
        }
        private void CleanerMode2(Bitmap bmp)
        {
            LastImage = ImageExplorer.NoiseCleaner(bmp, 2);
            EndOfMode();
        }
        private void TwoFrameMode(Bitmap bmp)
        {
            if (CounterZero(bmp)) return;

            Bitmap2 = bmp;
            Bitmap result = new Bitmap(Bitmap2.Width, Bitmap2.Height, Bitmap2.PixelFormat);
            FastImageCombine(Bitmap1, Bitmap2, result);
            LastImage = result;
            EndOfMode();
        }
        private void ThridMode(Bitmap bmp)
        {
            if (CounterZero(bmp)) return;
            if (CounterOne(bmp)) return;

            Bitmap3 = bmp;
            Bitmap result = new Bitmap(bmp.Width, bmp.Height, bmp.PixelFormat);   //= new Bitmap(bitmap2);
            FastImageCombine(MidleResult, Bitmap3, result);
            LastImage = result;
            EndOfMode();
        }
        private bool CounterZero(Bitmap bmp)
        {
            if (ModeCounter == 0)
            {
                Bitmap1 = bmp;
                ModeCounter++;
                return true;
            }
            else return false;
        }
        private bool CounterOne(Bitmap bmp)
        {
            if (ModeCounter == 1)
            {
                ModeCounter++;
                Bitmap2 = bmp;
                MidleResult = new Bitmap(bmp.Width, bmp.Height, bmp.PixelFormat);   // = new Bitmap(bitmap2);
                FastImageCombine(Bitmap1, Bitmap2, MidleResult);
                return true;
            }
            else return false;
        }
        private void EndOfMode()
        {
            ModeCounter = 0;
            Bitmap1 = null;
            Bitmap2 = null;
            Bitmap3 = null;
            ItReading = true;
            ItNew = true;
        }


        #region BitmapUpgrade
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bm1"></param>
        /// <param name="bm2"></param>
        /// <param name="result"></param>
        /// <returns>Возаращает количество критично отличающихся пикселей</returns>
        private static int FastImageCombine(Bitmap bm1, Bitmap bm2, Bitmap result)
        {
            int errCount = 0;
            if (bm1.Width != bm2.Width || bm1.Height != bm2.Height) return errCount;
            unsafe
            {
                BitmapData bitmapDataRes = result.LockBits(new Rectangle(0, 0, result.Width, result.Height), ImageLockMode.ReadWrite, result.PixelFormat);
                BitmapData bitmapData1 = bm1.LockBits(new Rectangle(0, 0, bm1.Width, bm1.Height), ImageLockMode.ReadWrite, bm1.PixelFormat);
                BitmapData bitmapData2 = bm2.LockBits(new Rectangle(0, 0, bm2.Width, bm2.Height), ImageLockMode.ReadWrite, bm2.PixelFormat);
                //try
                //{
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bm1.PixelFormat) / 8;
                int heightInPixels = bitmapData1.Height;
                int widthInBytes = bitmapData1.Width * bytesPerPixel;
                byte* ptrFirstPixel = (byte*)bitmapData1.Scan0;
                byte* ptrFirstPixel2 = (byte*)bitmapData2.Scan0;
                byte* ptrFirstPixelRes = (byte*)bitmapDataRes.Scan0;

                for (int y = 0; y < heightInPixels; y++)
                {
                    byte* currentLineBm1 = ptrFirstPixel + (y * bitmapData1.Stride);
                    byte* currentLineBm2 = ptrFirstPixel2 + (y * bitmapData1.Stride);
                    byte* currentLineBmRes = ptrFirstPixelRes + (y * bitmapData1.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                       int iValue = 0;
                        int t1 = (int)currentLineBm1[x];
                        int t2 = (int)currentLineBm2[x];
                        if (Math.Abs((int)currentLineBm1[x] - (int)currentLineBm2[x]) > 0)
                        {
                            iValue = ((int)currentLineBm1[x] > (int)currentLineBm2[x] ? (int)currentLineBm1[x] : (int)currentLineBm2[x]);
                            errCount++;
                        }
                        else iValue = (int)currentLineBm1[x];

                        //int iValue = Math.Abs((int)currentLineBm1[x] - (int)currentLineBm2[x]) > 0 ?
                        //    ((int)currentLineBm1[x] > (int)currentLineBm2[x] ? (int)currentLineBm1[x] : (int)currentLineBm2[x])
                        //    : (int)currentLineBm1[x];
                      


                        //int iBlue = Math.Abs((int)currentLineBm1[x] - (int)currentLineBm2[x]) > 0 ?
                        //    ((int)currentLineBm1[x] > (int)currentLineBm2[x] ? (int)currentLineBm1[x] : (int)currentLineBm2[x])
                        //    : (int)currentLineBm1[x];
                        //int iGreen = Math.Abs((int)currentLineBm1[x + 1] - (int)currentLineBm2[x + 1]) > 0 ?
                        //    ((int)currentLineBm1[x + 1] > (int)currentLineBm2[x + 1] ? (int)currentLineBm1[x + 1] : (int)currentLineBm2[x + 1])
                        //    : (int)currentLineBm1[x + 1];
                        //int iRed = Math.Abs((int)currentLineBm1[x + 2] - (int)currentLineBm2[x + 2]) > 0 ?
                        //    ((int)currentLineBm1[x + 2] > (int)currentLineBm2[x + 2] ? (int)currentLineBm1[x + 2] : (int)currentLineBm2[x + 2])
                        //    : (int)currentLineBm1[x + 2];
                        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        // calculate new pixel value
                        currentLineBmRes[x] = (byte)iValue;
                        currentLineBmRes[x + 1] = (byte)iValue;
                        currentLineBmRes[x + 2] = (byte)iValue;
                    }
                }
                result.UnlockBits(bitmapDataRes);
                bm1.UnlockBits(bitmapData1);
                bm2.UnlockBits(bitmapData2);
                return errCount;
                //}
                //finally
                //{
                //    if (bitmapDataRes != null)
                //    result.UnlockBits(bitmapDataRes);
                //    if (bitmapData1 != null)
                //        bm1.UnlockBits(bitmapData1);
                //    if (bitmapData2 != null)
                //        bm2.UnlockBits(bitmapData2);
                //}
            }
        }
        #endregion
    }
    public class MemoryManagerClient
    {
        /// <summary>
        /// Основная задача хранить ссылку в памяти для предотвращения сборки мусора на протяжении всей работы.
        /// Предпологается открытие двух управляемых участков памяти в обе стороны.
        /// При передаче сообщения в память, в начало сообщения отправляется управляющий символ который должен отличаться от полученного.
        /// Полученный символ - это символ который был отправлен в предыдущем сообщении.
        /// </summary>
        private MemoryMappedFile memoryServer;  // Camera Sick, image
        public int imageNumber = 0;
        private static bool isRunReader = false;
      
        public Bitmap Read()
        {
            try
            {

                if (isRunReader) return null;
                isRunReader = true;

                memoryServer = MemoryMappedFile.OpenExisting("MemoryServer");
                byte[] array_bitmap;
                int size, temp;

                using (MemoryMappedViewAccessor reader = memoryServer.CreateViewAccessor(0, 4, MemoryMappedFileAccess.Read))
                {
                    temp = reader.ReadInt32(0);
                }
                if (imageNumber == temp) { isRunReader = false; return null; }
                imageNumber = temp;


                using (MemoryMappedViewAccessor reader = memoryServer.CreateViewAccessor(4, 4, MemoryMappedFileAccess.Read))
                {
                    size = reader.ReadInt32(0);
                }
                using (MemoryMappedViewAccessor reader = memoryServer.CreateViewAccessor(8, size, MemoryMappedFileAccess.Read))
                {
                    array_bitmap = new byte[size];
                    reader.ReadArray<byte>(0, array_bitmap, 0, size);
                }

                isRunReader = false;
                return GetBitmap(array_bitmap);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Write(e.ToString());
                isRunReader = false;
                // Файл не найден "MemoryServer"
                return null;
            }
        }
       
        private static byte[] BitmapToByte(Bitmap bm)
        {
            using (var stream = new MemoryStream())
            {
                bm.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
                return stream.ToArray();
            }
        }

        private Bitmap GetBitmap(byte[] imageData)
        {
            using (var ms = new MemoryStream(imageData))
            {
                Bitmap bmp = new Bitmap(ms);
                return bmp;
            }
        }
    }
}
