﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Drawing.Imaging;
using System.Windows.Forms.DataVisualization.Charting;


namespace BagFinder
{
    public partial class Form1 : Form
    {
        #region Variables
        Memory memory = new Memory();
        DigitalPainter painter = new DigitalPainter();
        Form2 emulation = null;

        private static bool bCloseThread = false;
        private bool bCloseForm = false;
        private static bool bRunning = false;
        delegate void CloseApp();
        #endregion

        #region Properties
        public bool CloseThread
        {
            set { bCloseThread = value; }
            get { return bCloseThread; }
        }
        public bool CloseForm
        {
            set { bCloseForm = value; }
            get { return bCloseForm; }
        }
        #endregion

        #region TextBoxes
        public string StringX
        {
            set { tb_BegX.Text = value; }
            get
            {
                if (string.IsNullOrEmpty(tb_BegX.Text))
                {
                    tb_BegX.Text = "0";
                }
                return tb_BegX.Text;
            }
        }
        public string StringY
        {
            set { tb_BegY.Text = value; }
            get
            {
                if (string.IsNullOrEmpty(tb_BegY.Text))
                {
                    tb_BegY.Text = "0";
                }
                return tb_BegY.Text;
            }
        }
        public string StringW
        {
            set { tb_SizeW.Text = value; }
            get
            {
                if (string.IsNullOrEmpty(tb_SizeW.Text))
                {
                    tb_SizeW.Text = "0";
                }
                return tb_SizeW.Text; }
        }
        public string StringH
        {
            set { tb_SizeH.Text = value; }
            get
            {
                if (string.IsNullOrEmpty(tb_SizeH.Text))
                {
                    tb_SizeH.Text = "0";
                }
                return tb_SizeH.Text; }
        }
        #endregion


        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //memory.Start(this);
            CreateThreadLoopChangeImage();
        }

        #region MAIN

        // Первый единичный запуск второго потока
        public async void CreateThreadLoopChangeImage()
        {
            Thread imageThread = new Thread(new ThreadStart(LoopChangeImage));
            imageThread.Start(); // запускаем поток

            await Task.Delay(100);
            this.Invoke(new Action(() => Initialization()));
        }

        // Цикл перезаписи и обработки изображения с памяти
        public async void LoopChangeImage()
        {
            await Task.Delay(150);

            while (!CloseThread)
            {
                memory.ReadMemory();
                if (!bRunning)
                    UpdateImageAndUI();
            }

            CloseApp cls = InvokeClose;
            cls();
        }

        // Обработка изображения и запись в интерфейс
        private void UpdateImageAndUI()
        {
            bRunning = true;
            if (memory.ItNew && memory.ItReading)
            {
                //Debug(); 
                CopyOriginalImageToImageToShow();
                SetPictureBox1();
                memory.ItNew = false;
            }

            bRunning = false;
        }

        // Инициализация интерфейса(первый запуск)
        private void Initialization()
        {
            if (memory.LastImage != null)
                labelSize.Text = "W:" + memory.LastImage.Width + " | H:" + memory.LastImage.Height;
            tb_DelayRead.Text = memory.DelayRead.ToString();
            #region checkedListBoxModes
            comboBoxMode.Items.Clear();
            foreach (string name in Enum.GetNames(typeof(Memory.ReadingMode)))
                comboBoxMode.Items.Add(name);
            comboBoxMode.DropDownStyle = ComboBoxStyle.DropDownList;
            if (comboBoxMode.Items.Count != 0)
                comboBoxMode.SelectedItem = comboBoxMode.Items[0];
            if (painter != null)
                tb_Border.Text = painter.Border.ToString();
            if (painter != null)
                tb_MaxFrames.Text = painter.oLander.MaxFrames.ToString();
            #endregion
            UpdateRectSearch();
        }

        // Нарисовать Прямоугольную область поиска
        private void CopyOriginalImageToImageToShow()
        {
            memory.ImageToShow = new Bitmap(memory.LastImage);
            painter.Draw(memory.ImageToShow);
        }

        // Установка изображения в PictureBox при условии включенного ChekBox
        private void SetPictureBox1()
        {
            Bitmap bmp = memory.ImageToShow == null ? memory.LastImage : memory.ImageToShow;

            DataSetting.Display = new DisplayElements(chBoxDrawSD.Checked, chBoxDrawRecSearch.Checked, chBoxDrawLineOnBag.Checked, chBoxDrawBags.Checked);
            if (checkBoxUpdatePB.Checked)
                this.Invoke(new Action(() => pictureBox1.Image = bmp));
        }

        #endregion

        #region Close application

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;                // Stop Exit
            CloseThread = true;                  // Stop setcond Thread
            if (bCloseForm) e.Cancel = false;      // EXIT
        }

        private void InvokeClose()
        {
            bCloseForm = true;                                     // Access for EXIT for first Thread
            this.Invoke(new Action(() => this.Close()));    // Call Close Form1
        }

        #endregion

        #region ChackMax XYWH
        /// <summary>
        /// Возвращаемое значение не может ровняться максимуму(для задания ширины или высоты требуется минимум 1 пиксель).
        /// </summary>
        /// <param name="_str">Подставляются цифры с текстбокс плюс последний введенный знак</param>
        /// <param name="_max">Подставляется максимальное значение. Возвращаемое значение специфично.</param>
        /// <returns> Варинты: _str; _max-1; 0; </returns>
        private int CheckMaxXY(string _str, int _max)
        {
            if (string.IsNullOrEmpty(_str)) _str = "0";
            int l = Convert.ToInt32(_str);
            if (l >= 0 && l < _max)
                return l;
            else if (l >= _max)
                return _max - 1;
            else return 0;
        }
        /// <summary>
        /// Возвращаемое значение может ровняться максимуму.
        /// </summary>
        /// <param name="_str">Подставляются цифры с текстбокс плюс последний введенный знак.</param>
        /// <param name="_max">Подставляется максимальное значение. Возвращаемое значение специфично.</param>
        /// <returns> Варинты: _str; _max; 1; </returns>
        private int CheckMaxWH(string _str, int _max)
        {
            if (string.IsNullOrEmpty(_str)) _str = "1";
            int l = Convert.ToInt32(_str);
            if (l > 0 && l <= _max)
                return l;
            else if (l >= _max)
                return _max;
            else
                return 1;
        }
        #endregion

        #region Controls of SeachBox
        private void EventKeyPressBoxXY(TextBox sender, KeyPressEventArgs e, int max)
        {
            if (LimiterTextBoxXY(sender, e.KeyChar, max)) e.Handled = true;
        }
        private bool LimiterTextBoxXY(TextBox sender, char ch, int max)
        {
            string str = char.IsDigit(ch) ? sender.Text + ch : sender.Text;
            int val = CheckMaxXY(str, max);

            if (val == max - 1 || val == 0)
            {
                sender.Text = val.ToString();
                return true;
            }
            return false;
        }
        private void EventKeyPressBoxWH(TextBox sender, KeyPressEventArgs e, int max)
        {
            if (LimiterTextBoxWH(sender, e.KeyChar, max)) e.Handled = true;
        }
        private bool LimiterTextBoxWH(TextBox sender, char ch, int max)
        {
            string str = char.IsDigit(ch) ? sender.Text + ch : sender.Text;
            int val = CheckMaxWH(str, max);

            if (val == max || val == 1)
            {
                sender.Text = val.ToString();
                return true;
            }
            return false;
        }
        #endregion

        #region Update Width Height
        private void UpdateWidth()
        {
            if (memory.LastImage != null)
                LimiterTextBoxWH(tb_SizeW, ' ', memory.LastImage.Width - Convert.ToInt16(StringX));
        }
        private void UpdateHeight()
        {
            if (memory.LastImage != null)
                LimiterTextBoxWH(tb_SizeH, ' ', memory.LastImage.Height - Convert.ToInt16(StringY));
        }
        #endregion

        #region Events KeyPress for TextBox
        private void Tb_MaxFrames_TextChanged(object sender, EventArgs e)
        {
            int value = painter.oLander.MaxFrames;
            bool itParsed = int.TryParse(tb_MaxFrames.Text, out value);
            if (itParsed && value > 0 && value < 1500) painter.oLander.MaxFrames = value;
            else tb_MaxFrames.Text = painter.oLander.MaxFrames.ToString();
        }
        private void Tb_Border_KeyUp(object sender, KeyEventArgs e)
        {
            float newBorder = 0;
            if (float.TryParse(tb_Border.Text, out newBorder))
            {
                painter.Border = newBorder;
            }
            else if (tb_Border.Text.Length > 0) tb_Border.Text = tb_Border.Text.Remove(tb_Border.Text.Length - 1, 1);
        }
        private void Tb_BegX_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
            else if (char.IsDigit(e.KeyChar) && memory.LastImage != null)
                EventKeyPressBoxXY((TextBox)sender, e, memory.LastImage.Width);
            UpdateRectSearch();
        }

        private void Tb_BegY_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
            else if (char.IsDigit(e.KeyChar) && memory.LastImage != null)
                EventKeyPressBoxXY((TextBox)sender, e, memory.LastImage.Height);
            UpdateRectSearch();
        }

        private void Tb_SizeW_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
            else if (char.IsDigit(e.KeyChar) && memory.LastImage != null)
            {
                int max = memory.LastImage.Width - Convert.ToInt16(StringX);
                EventKeyPressBoxWH((TextBox)sender, e, max);
            }
            UpdateRectSearch();
        }

        private void Tb_SizeH_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
            else if (char.IsDigit(e.KeyChar) && memory.LastImage != null)
            {
                int max = memory.LastImage.Height - Convert.ToInt16(StringY);
                EventKeyPressBoxWH((TextBox)sender, e, max);
            }
            UpdateRectSearch();
        }

        private void UpdateRectSearch()
        {
            int x, y, w, h;
            bool xAccess = int.TryParse(StringX, out x);
            bool yAccess = int.TryParse(StringY, out y);
            bool wAccess = int.TryParse(StringW, out w);
            bool hAccess = int.TryParse(StringH, out h);
            if (xAccess && yAccess && wAccess && hAccess)
            {
                painter.RectSearch = new Rectangle(x, y, w, h);
            }
        }
        #endregion

        #region BegXY KeyUp
        private void Tb_BegX_KeyUp(object sender, KeyEventArgs e)
        {
            UpdateWidth();
        }
        private void Tb_BegY_KeyUp(object sender, KeyEventArgs e)
        {
            UpdateHeight();
        }
        #endregion

        #region Buttons
        private void BGrid_Click(object sender, EventArgs e)
        {
            panelMenu.Hide();
            panelDrawing.Hide();
            panelGrid.Show();
        }

        private void BMenu_Click(object sender, EventArgs e)
        {
            panelMenu.Show();
            panelGrid.Hide();
        }

        private void BDrawingPanel_Click(object sender, EventArgs e)
        {
            panelDrawing.Show();
        }
        private void bt_emulation_Click(object sender, EventArgs e)
        {
            if (emulation != null) { emulation.Close(); emulation = null; }

            emulation = new Form2();
            emulation.Show();
            PictureBox pbox = new PictureBox();
            pbox.Location = new Point(10, 10);
            Size sizeForm2 = emulation.Size;
            Size sizePbox = new Size(sizeForm2.Width - 40, sizeForm2.Height - 80);

            pbox.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top);
            pbox.SizeMode = PictureBoxSizeMode.StretchImage;
            pbox.Size = sizePbox;

            int width = 350;
            int height = 350;
            Bitmap Bmp = new Bitmap(width, height);
            using (Graphics gfx = Graphics.FromImage(Bmp))
            using (SolidBrush brush = new SolidBrush(Color.FromArgb(50, 80, 100)))
            {
                gfx.FillRectangle(brush, 0, 0, width, height);
            }

            emulation.Controls.Add(pbox);
            pbox.Image = Bmp;
            emulation.StartEmulation(pbox);
            emulation.Width = width;
            emulation.Height = height;
        }
        private void Button_GridPix_Click(object sender, EventArgs e)
        {
            panelCells1.Visible = true;
            chart1.Visible = false;
        }
        private void Button_Graph_Click(object sender, EventArgs e)
        {
            chart1.Visible = true;
            panelCells1.Visible = false;
        }
        private void bt_ClearGrid_Click(object sender, EventArgs e)
        {
            if (chart1.Series != null && chart1.Series.Count > 0)
            {
                chart1.Series.Clear();
            }
        }
        #endregion

        #region WindowsFormEvents
        private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            Bitmap bitmap = (Bitmap)pictureBox1.Image;
            Point cursor = GetCursorPosition(bitmap, e);
            Color pixel = bitmap.GetPixel(cursor.X, cursor.Y);

            labelPixelColor.Text = cursor +" | " + pixel;
            //DebugPixel(cursor, bitmap);
        }
        private void PictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            Bitmap bitmap = (Bitmap)pictureBox1.Image;
            Point cursor = GetCursorPosition(bitmap,e);
            Color pixel = bitmap.GetPixel(cursor.X, cursor.Y);

            UpdatePanelCells(bitmap, cursor);
            ConstructChart(cursor);
            //ShowTwoVectorDepthFromCenter(cursor, bitmap, false);
        }
        private Point GetCursorPosition(Bitmap bmp, MouseEventArgs e)
        {
            Point cursor = new Point();
            cursor.X = e.X * bmp.Width / pictureBox1.Width;
            cursor.Y = e.Y * bmp.Height / pictureBox1.Height;
            return cursor;
        } 
        private void ComboBoxMode_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBoxMode.SelectedItem != null)
                memory.ReadMode = (Memory.ReadingMode)Enum.Parse(typeof(Memory.ReadingMode), (string)comboBoxMode.SelectedItem);
        }
        private void Tb_DelayRead_TextChanged(object sender, EventArgs e)
        {
            int value = memory.DelayRead;
            bool itParsed = int.TryParse(tb_DelayRead.Text, out value);
            if (itParsed && value > 0 && value < 10000) memory.DelayRead = value;
            else tb_DelayRead.Text = memory.DelayRead.ToString();
        }
       
        #endregion

        #region Test
        private void ConstructChart(Point p)
        {
            float[] arr = painter.GetDepthSpeedLineByY(new Bitmap(memory.LastImage), painter.RectSearch, p.Y);
            for (int i = 0; i <arr.Length; i++)
                AddPointByY_Chart1(p, arr[i]);
        }

        private void AddPointByY_Chart1(Point p, float value)
        {
            ChartArea chart = new ChartArea();
            if (chart1.Series.FindByName("Y:" + p.Y) == null)
            {
                chart1.Series.Add("Y:" + p.Y);
                chart1.Series["Y:" + p.Y].ChartType = SeriesChartType.Line;
                //chart1.Series["Y:" + p.Y].ChartArea = chart;
                chart1.Series["Y:" + p.Y].Points.AddXY((float)1, value);
            }
            else
            {
                int length = chart1.Series["Y:" + p.Y].Points.Count; length++;
                chart1.Series["Y:" + p.Y].Points.AddXY((float)length, value);
            }
        }
        #endregion

        #region UserControl
        private void UpdatePanelCells(Bitmap bmp, Point cursor)
        {
            if (panelCells1.CellControls == null || panelCells1.CellControls.Length <= 1 || panelCells1.CellControls[0].Length <= 1) return;   // Exit

            int countV = panelCells1.CellControls.Length;
            int countH = panelCells1.CellControls[0].Length;
            int centerH = countH / 2;
            int centerV = countV / 2;

            for (int i = 0; i < countV; i++)
                for (int j = 0; j < countH; j++)
                {
                    Point p = new Point(cursor.X - centerH, cursor.Y - centerV);
                    p.X += j;
                    p.Y += i;
                    int depth = bmp.GetPixel(p.X,p.Y).B;

                    if (centerH == j && centerV == i)
                        panelCells1.CellControls[i][j].BackColor = Color.GreenYellow;
                    panelCells1.CellControls[i][j].label_Cell.Text = "" + depth;//"X:" + p.X + "\n" + "Y:" + p.Y + "\n" + "D:" + depth;

                }
        }
        #endregion

        private void Button1_Click(object sender, EventArgs e)
        {
            Point A = new Point(6, 2);
            Point B = new Point(2, 5);

            Point C = new Point(7, 3);
            Point D = new Point(10, 7);

            Point p = Geometry.Intersection(A,B,C,D);
            System.Diagnostics.Debug.Write("" + A + B + C + D + " = \n" + p + "\n");
        }
    }
}
