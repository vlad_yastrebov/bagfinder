﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Reflection;

namespace BagFinder
{

    public class DigitalPainter : TimeDerivative
    {

        #region Variables
        private Rectangle rect = new Rectangle();
        private LineManagerForBag manager = new LineManagerForBag();
        private Point[][] pointsOfSides = null;         // 0 left 1 up 2 right 3 down
        private float fBorder = 0.11f;



        #region Properties
        public LineManagerForBag oLander
        {
            get { return manager; }
            set { manager = value; }
        }
        public Rectangle RectSearch
        {
            set { rect = value; }
            get { return rect; }
        }
        public Point[][] PointsOfSides
        {
            get { return pointsOfSides; }
            set { pointsOfSides = value; }
        }
        public float Border
        {
            get { return fBorder; }
            set { fBorder = value; }
        }
        #endregion

        #endregion


        public DigitalPainter()
        {
            Initialization();
        }
        public void Initialization()
        {
            DataSetting.Display = new DisplayElements(true, true, true, true);
        }


        public void Draw(Bitmap _bmp)
        {
            FindMinDepth(_bmp, RectSearch);   // Определение самой близкой точкой.
            Point[][] arr = GetDepthSpeedPoints(_bmp, RectSearch, fBorder);
            PointsOfSides = arr;

            List<LineOnBag> lobList = manager.ModifiedBagLines(arr);
            manager.UpdateLongSides(arr,LowDepth);
            if (DataSetting.Display.DrawSpeedDepth) DrawDepthSpeedPoints(_bmp, arr);

            using (Graphics gr = Graphics.FromImage(_bmp))
            {
                if (DataSetting.Display.DrawReactSearch) DrawSearchBox(gr, Pens.Blue, RectSearch, _bmp.Size);
                if (DataSetting.Display.DrawLineOnBag)
                {
                    DrawLongSides(gr, Pens.Blue, RectSearch, _bmp.Size, manager);
                    DrawLines(gr, Pens.Coral, lobList);
                }
                if (DataSetting.Display.DrawBags) DrawBag(gr, Pens.Green, manager.ArrayOfBag);
            }
        }
        public void DrawBag(Graphics _gr, Pen _pen, Bag[] bags)
        {
            if (bags == null) return;

            foreach(Bag b in bags)
            {
                Point[] pBag = b.GetPoints;
                //sides
                _gr.DrawLine(_pen, pBag[0], pBag[1]);
                _gr.DrawLine(_pen, pBag[1], pBag[2]);
                _gr.DrawLine(_pen, pBag[2], pBag[3]);
                _gr.DrawLine(_pen, pBag[3], pBag[0]);
                //center
                _gr.DrawLine(_pen, pBag[0], pBag[2]);
                _gr.DrawLine(_pen, pBag[1], pBag[3]);
            }
        }
        public void DrawLongSides(Graphics _gr, Pen _pen, Rectangle _rect, Size size, LineManagerForBag ol)
        {
            if (ol == null || ol.LongSides == null) return;

            Size sizeWindow = new Size(176,144);

            // Create string to draw.
            string left = "L:" + Math.Round(ol.LongSides[(int)Sides.Left],3).ToString();
            string right = "R:" + Math.Round(ol.LongSides[(int)Sides.Right],3).ToString();
            string up = "U:" + Math.Round(ol.LongSides[(int)Sides.Up],3).ToString();
            string down = "D:" + Math.Round(ol.LongSides[(int)Sides.Down],3).ToString();

            // Create font and brush.
            Font drawFont = new Font("Arial", 8);
            SolidBrush drawBrush = new SolidBrush(Color.Crimson);

            Point posLeft = new Point(0, sizeWindow.Height -15);
            Point posRight = new Point(sizeWindow.Width - 50, 0);
            Point posUp = new Point(sizeWindow.Width / 2-20, 0);
            Point posDown = new Point(sizeWindow.Width / 2 -20, sizeWindow.Height - 15);
            // Create point for upper-left corner of drawing.


            // Set format of string.
            StringFormat drawFormat = new StringFormat();
         //   drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;

            // Draw string to screen.
            _gr.DrawString(left, drawFont, drawBrush, posLeft.X, posLeft.Y, drawFormat);
            _gr.DrawString(right, drawFont, drawBrush, posRight.X, posRight.Y, drawFormat);
            _gr.DrawString(up, drawFont, drawBrush, posUp.X, posUp.Y, drawFormat);
            _gr.DrawString(down, drawFont, drawBrush, posDown.X, posDown.Y, drawFormat);
        }
        public void DrawSearchBox(Graphics _gr, Pen _pen, Rectangle _rect, Size size)
        {
            int x = _rect.X - 1 >= 0 ? _rect.X - 1 : _rect.X;
            int y = _rect.Y - 1 >= 0 ? _rect.Y - 1 : _rect.Y;
            int w = _rect.Width + _rect.X + 2 <= size.Width ? _rect.Width + 2 : _rect.Width;
            int h = _rect.Height + _rect.Y + 2 <= size.Height ? _rect.Height + 2 : _rect.Height;
            Rectangle rec = new Rectangle(x,y,w,h);
            _gr.DrawRectangle(_pen, rec);
        }
        public void DrawLines(Graphics _gr, Pen _pen, List<LineOnBag> lobList)
        {
            if (lobList == null) return;
            
            foreach (LineOnBag lob in lobList)
            {
                _gr.DrawLine(_pen, lob.A, lob.B);
            }
        }
        public void DrawDepthSpeedPoints(Bitmap bitmapResult, Point[][] arrPoints)
        {
            if (arrPoints == null) return;
            unsafe
            {
                BitmapData bitmapData1 = bitmapResult.LockBits(new Rectangle(0, 0, bitmapResult.Width, bitmapResult.Height),
                    ImageLockMode.ReadWrite, bitmapResult.PixelFormat);
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bitmapResult.PixelFormat) / 8;
                byte* ptrFirstPixel = (byte*)bitmapData1.Scan0;
                for (int i = 0; i < arrPoints.Length; i++)
                    for (int j = 0; j < arrPoints[i].Length; j++)
                    {
                        byte* currentLineBm1 = ptrFirstPixel + (arrPoints[i][j].Y * bitmapData1.Stride);
                        int indexX = bytesPerPixel * arrPoints[i][j].X;
                        currentLineBm1[indexX] = 0;
                        currentLineBm1[indexX + 1] = 0;
                        currentLineBm1[indexX + 2] = 255;
                    }
                bitmapResult.UnlockBits(bitmapData1);
            }
        }


        /// <summary>
        /// Поиск точек перехода с пола на мешки.
        /// </summary>
        /// <param name="bmp">картинка</param>
        /// <param name="rectSearch">область поиска </param>
        /// <param name="fBorder">скорость перепада для сохранения точки</param>
        /// <returns></returns>
        public Point[][] GetDepthSpeedPoints(Bitmap bmp, Rectangle rectSearch, float fBorder)
        {
            if (RectSearch == new Rectangle()) return null;

            int iAxis = 0;
            int iAxisLimiter = 0;
            float[] fDepthSpeed;
            Point[][] array = new Point[4][]; // Directions
            array[(int)Sides.Left] = new Point[rectSearch.Height];      //0 index
            array[(int)Sides.Right] = new Point[rectSearch.Height];     //2 index
            array[(int)Sides.Up] = new Point[rectSearch.Width];         //1 index
            array[(int)Sides.Down] = new Point[rectSearch.Width];       //3 index

            for (int iGroup = 0; iGroup < 2; iGroup++)
            { 

                if (iGroup == 0) { iAxis = rectSearch.Y; iAxisLimiter = iAxis + rectSearch.Height; }
                else { iAxis = rectSearch.X; iAxisLimiter = iAxis + rectSearch.Width; }

                int iIndexBeg = 0;
                int iIndexEnd = 0;
                int oldSpacing = PixelSpacing;
                for (; iAxis < iAxisLimiter; iAxis++)
                {
                    if (iGroup == 0) PixelSpacing = 4;
                    else PixelSpacing = oldSpacing;
                    if (iGroup == 0)    // left right
                        fDepthSpeed = GetDepthSpeedLineByY(bmp, rectSearch, iAxis, LowDepth, 4);
                    else
                        fDepthSpeed = GetDepthSpeedLineByX(bmp, rectSearch, iAxis, LowDepth, 4);

                    int iPointBeg = 0;
                    int iPointEnd = fDepthSpeed.Length-1;  
                    int directBeg = (int)(iGroup == 0 ? Sides.Left : Sides.Up);
                    int directEnd = (int)(iGroup == 0 ? Sides.Right : Sides.Down);
                   
                    for (; iPointBeg < fDepthSpeed.Length && iIndexBeg < array[iGroup].Length;)
                    {
                        if (Math.Abs(fDepthSpeed[iPointBeg]) >= fBorder)
                        {
                            if (iGroup == 0)
                            {
                                array[directBeg][iIndexBeg].X = rectSearch.X + iPointBeg + PixelSpacing;
                                array[directBeg][iIndexBeg].Y = iAxis;
                            }
                            else
                            {
                                array[directBeg][iIndexBeg].X = iAxis;
                                array[directBeg][iIndexBeg].Y = rectSearch.Y + iPointBeg + PixelSpacing;
                            }
                            iIndexBeg++;
                            iPointBeg++;
                            break;
                        }
                        else iPointBeg++;
                    }
                    for (; iPointEnd > 0 && iIndexEnd < array[iGroup].Length;)
                    {
                        if (Math.Abs(fDepthSpeed[iPointEnd]) >= fBorder)
                        {
                            if (iGroup == 0)
                            {
                                array[directEnd][iIndexEnd].X = rectSearch.X + iPointEnd + PixelSpacing;
                                array[directEnd][iIndexEnd].Y = iAxis;
                            }
                            else
                            {
                                array[directEnd][iIndexEnd].X = iAxis;
                                array[directEnd][iIndexEnd].Y = rectSearch.Y + iPointEnd + PixelSpacing;
                            }
                            iIndexEnd++;
                            iPointEnd--;
                            break;
                        }
                        else iPointEnd--;
                    }
                }

            }
            return array;
        }

    }


    public class TimeDerivative
    {
        #region Variables
        private int _lowDepth = 255;            // самая высокая точка(за исключением 0)
        private int iPixelSpacing = 3;



        #region Property
        public int PixelSpacing
        {
            get { return iPixelSpacing; }
            set { iPixelSpacing = value; }
        }

        public int LowDepth { get { return _lowDepth; } set { _lowDepth = value; } }
        #endregion

        #endregion



        #region Functions
        /// <summary>
        /// Поиск самой высокой точки на мешке(Поиск минимальной глубины). Поиск на участке заданном пользователем в аргументе. 
        /// </summary>
        /// <param name="bmp">изображение на котором идет поиск</param>
        /// <param name="rectSearch">область поиска</param>
        protected void FindMinDepth(Bitmap bmp, Rectangle rectSearch)
        {
            unsafe
            {
                LowDepth = 255;
                Bitmap bitmapResult = bmp.Clone(new Rectangle(0, 0, bmp.Width, bmp.Height), bmp.PixelFormat);
                BitmapData bitmapData1 = bitmapResult.LockBits(new Rectangle(0, 0, bitmapResult.Width, bitmapResult.Height), ImageLockMode.ReadWrite, bitmapResult.PixelFormat);
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bmp.PixelFormat) / 8;
                int heightInPixels = rectSearch.Height + rectSearch.Y;
                int widthInBytes = rectSearch.X * bytesPerPixel + rectSearch.Width * bytesPerPixel; //bitmapData1.Width * bytesPerPixel;
                byte* ptrFirstPixel = (byte*)bitmapData1.Scan0;

                for (int y = rectSearch.Y; y < heightInPixels; y++)
                {
                    byte* currentLineBm1 = ptrFirstPixel + (y * bitmapData1.Stride);
                    for (int x = rectSearch.X * bytesPerPixel; x < widthInBytes; x = x + bytesPerPixel)
                        UpdateLowDepth(currentLineBm1[x]);

                }
                bitmapResult.UnlockBits(bitmapData1);
            }
        }

        /// <summary>
        /// Возвращает массив производных выбронной точки Y параллельной оси X
        /// </summary>
        /// <param name="bmp"></param>
        /// <param name="indexY"></param>
        /// <returns></returns>
        public float[] GetDepthSpeedLineByY(Bitmap bmp, Rectangle rectSearch, int indexY, int iStandart = 0, int iMaxOffset = 5)
        {
            unsafe
            {
                Bitmap bitmapResult = bmp.Clone(new Rectangle(0, 0, bmp.Width, bmp.Height), bmp.PixelFormat);
                BitmapData bitmapData1 = bitmapResult.LockBits(new Rectangle(0, 0, bitmapResult.Width, bitmapResult.Height), ImageLockMode.ReadWrite, bitmapResult.PixelFormat);
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bmp.PixelFormat) / 8;

                int x0 = rectSearch.X * bytesPerPixel;
                int x1 = rectSearch.X * bytesPerPixel + PixelSpacing * bytesPerPixel; // end
                int heightInPixels = bitmapData1.Height;
                int widthInBytes = rectSearch.X * bytesPerPixel + rectSearch.Width * bytesPerPixel; //bitmapData1.Width * bytesPerPixel;
                int widthInBytesGlobal = bitmapData1.Width * bytesPerPixel; //bitmapData1.Width * bytesPerPixel;
                int arrSize = (rectSearch.X + rectSearch.Width + PixelSpacing) <= bmp.Width ? rectSearch.X + rectSearch.Width + PixelSpacing : bmp.Width;
                float[] arr = new float[arrSize];
                byte* ptrFirstPixel = (byte*)bitmapData1.Scan0;
                byte* currentLineBm1 = ptrFirstPixel + (indexY * bitmapData1.Stride);
                int i = 0;
                for ( ; x0 < widthInBytes && x1 < widthInBytesGlobal; x0 = x0 + bytesPerPixel, x1 = x1 + bytesPerPixel)
                {
                    int s0 = currentLineBm1[x0];
                    int s1 = currentLineBm1[x1];
                    if (iStandart != 0 && Math.Abs(iStandart - s1) > iMaxOffset) arr[i] = 0.1f;
                    else arr[i] = (float)((s1 - s0) / ((float)PixelSpacing + 1));
                    i++;
                }

                bitmapResult.UnlockBits(bitmapData1);
                return arr;
            }
        }

        /// <summary>
        /// Возвращает массив производных выбронной точки X параллельной оси Y
        /// </summary>
        /// <param name="bmp"></param>
        /// <param name="indexY"></param>
        /// <returns></returns>
        public float[] GetDepthSpeedLineByX(Bitmap bmp, Rectangle rectSearch, int indexX, int iStandart = 0, int iMaxOffset = 5)
        {
            unsafe
            {
                Bitmap bitmapResult = bmp.Clone(new Rectangle(0, 0, bmp.Width, bmp.Height), bmp.PixelFormat);
                BitmapData bitmapData1 = bitmapResult.LockBits(new Rectangle(0, 0, bitmapResult.Width, bitmapResult.Height), ImageLockMode.ReadWrite, bitmapResult.PixelFormat);
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bmp.PixelFormat) / 8;

                int Y;
                int arrSize = rectSearch.Y + rectSearch.Height + PixelSpacing < bmp.Height ? rectSearch.Height : rectSearch.Height - PixelSpacing;
                float[] arr = new float[arrSize];
                byte* ptrFirstPixel = (byte*)bitmapData1.Scan0;
                byte* currentLineBm1, currentLineBm2;
                for (int line = rectSearch.Y, index = 0; line < (rectSearch.Y+rectSearch.Height) && 
                    line < (rectSearch.Y + rectSearch.Height + PixelSpacing + 1); line++, index++)
                {
                    currentLineBm1 = ptrFirstPixel + (line * bitmapData1.Stride);
                    currentLineBm2 = ptrFirstPixel + ((line + PixelSpacing) * bitmapData1.Stride);
                    Y = bytesPerPixel * indexX;
                    int s0 = currentLineBm1[Y];
                    int s1 = currentLineBm2[Y];
                    if (iStandart != 0 && Math.Abs(iStandart - s1) > iMaxOffset) arr[index] = 0.1f;
                    else
                        arr[index] = (float)((s1 - s0) / ((float)PixelSpacing + 1));
                }

                bitmapResult.UnlockBits(bitmapData1);
                return arr;
            }
        }

        /// <summary>
        /// Сравнение заданной глубины с текущей и её замены если число меньше но больше 0
        /// </summary>
        /// <param name="depth">заданная глубина</param>
        public void UpdateLowDepth(byte depth)
        {
            LowDepth = depth > 0 && depth < LowDepth ? depth : LowDepth;
        }
        #endregion
    }


  
}
