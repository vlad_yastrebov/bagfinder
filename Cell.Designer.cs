﻿namespace BagFinder
{
    partial class Cell
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_Cell = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_Cell
            // 
            this.label_Cell.AutoSize = true;
            this.label_Cell.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_Cell.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Cell.Location = new System.Drawing.Point(5, 5);
            this.label_Cell.Name = "label_Cell";
            this.label_Cell.Size = new System.Drawing.Size(41, 41);
            this.label_Cell.TabIndex = 0;
            this.label_Cell.Text = "X:** \r\nY:**\r\nDepth:";
            // 
            // Cell
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.label_Cell);
            this.Name = "Cell";
            this.Size = new System.Drawing.Size(50, 50);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label label_Cell;
    }
}
