﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace BagFinder
{

    struct SpeedInfo
    {
        public int pCount;     // проверенных в пределах погрешности       Min - минимальное количество указано в глобальных переменных
        public int apCount;    // проверенных с нулевой погрешностью       
        public int begIndex;   // индекс начала
        public int endIndex;   // индекс конца
    }



    public class LineManagerForBag
    {
        #region Variables

        private LineOnBag[][] _arrayLOB = null;
        private LineOnBag[][] _arrayLOBview = null;
        private Bag[] _bags = null;
        private double[] _longSides = new double[Enum.GetValues(typeof(Sides)).Length];       // Результат вычисление длины в см
        private int _counterFrames = 0;
        private int _maxFrames = 15;                    // Количество подходов накладывания фреймов друг на друга 
        private float _fMaxSamplingErrorForLOB = 1f;
        private float _fMaxSpacingErrorForLOB = 3f;     // Максимальное допустимое расстояние между двумя отрезками для конкатенации
        // справедливо для прямых линий
        private int _iMaxSamplingError = 2;          // максимальное резрешимая ошибка глубины относительно соседнего пикселя
        private int _iMinLengthLine = 12;       // минимальная длина короткой линии в пикселях.
        // справедливо для кривых линий и прямых линий
        private float _fMaxErrorOfSpeed = 0.5f;      // максимальное отклонение изменения скорости

        private float _fMaxDistanceForConnect = 2f;
        private float _fMaxDistanceForConnectCorner = 8f;


        public Bag[] ArrayOfBag
        {
            get { return _bags; }
            set { _bags = value; }
        }
        public LineOnBag[][] ArrayLineOnBag
        {
            get { return _arrayLOB; }
            set { _arrayLOB = value; }
        }   // for Modificate
        public LineOnBag[][] ArrayLineOnBagToView // result
        {
            get { return _arrayLOBview; }
            set { _arrayLOBview = value; }
        }   // for Modificate
        public double[] LongSides
        {
            get { return _longSides; }
            set { _longSides = value; }
        }
        public float MaxSamplingErrorForLOB
        {
            get { return _fMaxSamplingErrorForLOB; }
            set { _fMaxSamplingErrorForLOB = value; }
        }
        public float MaxSpacingErrorForLOB
        {
            get { return _fMaxSpacingErrorForLOB; }
            set { _fMaxSpacingErrorForLOB = value; }
        }
        public int CounterFrames
        {
            get { return _counterFrames; }
            set { _counterFrames = value; }
        }
        public int MaxFrames
        {
            get { return _maxFrames; }
            set { _maxFrames = value; }
        }
        // справедливо для прямых линий
        public int MaxSamplingError
        {
            get { return _iMaxSamplingError; }
            set { _iMaxSamplingError = value; }
        }
        public int MinLengthShortLine
        {
            get { return _iMinLengthLine; }
            set { _iMinLengthLine = value; }
        }
        //справедливо для кривых линий и прямых линий
        public float MaxErrorOfSpeed
        {
            get { return _fMaxErrorOfSpeed; }
            set { _fMaxErrorOfSpeed = value; }
        }
        public float MaxDistanceForConnect
        {
            get { return _fMaxDistanceForConnect; }
            set { _fMaxDistanceForConnect = value; }
        }
        public float MaxDistanceForConnectCorner
        {
            get { return _fMaxDistanceForConnectCorner; }
            set { _fMaxDistanceForConnectCorner = value; }
        }
        #endregion

        private List<LineOnBag> LineMerging(LineOnBag[][] arrayLob)
        {
            List<LineOnBag> list = new List<LineOnBag>();

            foreach (Sides side in Enum.GetValues(typeof(Sides)))
                for (int i = 0; i < arrayLob[(int)side].Length; i++)
                    for (int g = i + 1; g < arrayLob[(int)side].Length; g++)
                    {
                        //сравнение всех линии друг с другом
                        if (LineOnBag.LineDistanceLess(arrayLob[(int)side][i], arrayLob[(int)side][g], MaxDistanceForConnect))
                        {
                            arrayLob[(int)side][g] = LineOnBag.CombineLines(arrayLob[(int)side][i], arrayLob[(int)side][g], MaxDistanceForConnect);
                            arrayLob[(int)side][i].iCount = 0;
                        }
                    }
            arrayLob = LineOnBag.CleaningArrayOfLines(arrayLob);
            arrayLob = ConnectCornersAndCreateBags(arrayLob);

            // Counter Frames
            UpdateArrayLineOnBag(arrayLob);
            ArrayLineOnBagToView = ArrayLineOnBag;

            if (ArrayLineOnBagToView != null)
                foreach (Sides side in Enum.GetValues(typeof(Sides)))
                    foreach (LineOnBag lob in ArrayLineOnBagToView[(int)side]) list.Add(lob);

            return list;
        }

        /// <summary>
        /// Обновление длины каждой стороны на основе определенных камерой точек перехода на мешок.
        /// </summary>
        /// <param name="depthSpeed">точки перехода на мешок</param>
        /// <param name="lowDepth">самая высокая точка мешка</param>
        public void UpdateLongSides(Point[][] depthSpeed, int lowDepth)
        {
            if (depthSpeed == null) return;

            SizeF bag = new SizeF(37, 60);   // Размеры мешка
            double dDistBag = lowDepth * 1.2244897d;
            for (int side = 0; side < Enum.GetValues(typeof(Sides)).Length; side++)
            {
                int iPixelLength = depthSpeed[side].Length;
                LongSides[side] = (iPixelLength * dDistBag) / lowDepth;               //((double)lowDepth / FocalLens) * iPixelLength;

            }
        }


        /// <summary>
        /// Соединяет граничащие с разных сторон ближайшие прямые в точке пересечения.
        /// </summary>
        /// <param name="arrayLob"></param>
        /// <returns></returns>
        private LineOnBag[][] ConnectCornersAndCreateBags(LineOnBag[][] arrayLob)
        {
            // Сравнение выбронной стороны с двумя прилегающими
            for (int sideHoriz = 0; sideHoriz < Enum.GetValues(typeof(Sides)).Length; sideHoriz += 2)       // Left Right
                for (int sideVert = 1; sideVert < Enum.GetValues(typeof(Sides)).Length; sideVert += 2)   // Up Down
                    for (int i = 0; i < arrayLob[sideHoriz].Length; i++)
                        for (int j = 0; j < arrayLob[sideVert].Length; j++)
                        {
                            if (LineOnBag.LineDistanceLess(arrayLob[sideHoriz][i], arrayLob[sideVert][j], MaxDistanceForConnectCorner))
                            {
                                Tuple<Point, Point> line1 = Geometry.ResizeLine(arrayLob[sideHoriz][i].A, arrayLob[sideHoriz][i].B);
                                Tuple<Point, Point> line2 = Geometry.ResizeLine(arrayLob[sideVert][j].A, arrayLob[sideVert][j].B);
                                Point p0 = Geometry.Intersection(line1.Item1, line1.Item2, line2.Item1, line2.Item2);

                                // Пересечение прямых должно быть в пределах кадра
                                if (p0.IsEmpty || p0.X >= 176 || p0.Y >= 144 || p0.X < 0 || p0.Y < 0) continue;

                                Bag bag;
                                Point a, b, c;
                                b = p0;

                                if (Geometry.GetDistance(arrayLob[sideHoriz][i].A, p0) < MaxDistanceForConnectCorner) { arrayLob[sideHoriz][i].A = p0; a = arrayLob[sideHoriz][i].B; arrayLob[sideHoriz][i].bCornerA = true; }
                                else { arrayLob[sideHoriz][i].B = p0; a = arrayLob[sideHoriz][i].A; arrayLob[sideHoriz][i].bCornerB = true; }
                                if (Geometry.GetDistance(arrayLob[sideVert][j].A, p0) < MaxDistanceForConnectCorner) { arrayLob[sideVert][j].A = p0; c = arrayLob[sideVert][j].B; arrayLob[sideVert][j].bCornerA = true; }
                                else { arrayLob[sideVert][j].B = p0; c = arrayLob[sideVert][j].A; arrayLob[sideVert][j].bCornerB = true; }


                                bool bBagCreated = Bag.CreateBag(a, b, c, out bag, (Sides)sideHoriz, (Sides)sideVert);

                                if (bBagCreated)
                                    ArrayOfBag = Bag.CheckAndAddBag(bag, ArrayOfBag);

                                if (ArrayOfBag != null && ArrayOfBag.Length > 1)
                                    ArrayOfBag = Bag.RemoveDuplicateBags(ArrayOfBag);

                                ArrayOfBag = Bag.BagsFit(ArrayOfBag);
                            }
                        }
            return arrayLob;
        }

        /// <summary>
        /// Доработка предыдущих линий с помощью нового массива линий.
        /// </summary>
        /// <param name="arrayLob"></param>
        /// <returns></returns>
        private LineOnBag[][] ConnectLines(LineOnBag[][] arrayLob)
        {
            if (ArrayLineOnBag == null) { ArrayLineOnBag = arrayLob; return ArrayLineOnBag; }
            //  ResizeLine
            //  IsEmptyArea
            //  PointInTheLine
            //  GetDistance
            for (int side = 0; side < Enum.GetValues(typeof(Sides)).Length; side++)
            {
                if (ArrayLineOnBag[side].Length == 0) ArrayLineOnBag[side] = arrayLob[side];

                for (int i = 0; i < ArrayLineOnBag[side].Length; i++)
                    for (int j = 0; j < arrayLob[side].Length; j++)
                    {
                        LineOnBag lob;
                        if (LineOnBag.IsSuccessfullyCombined(arrayLob[side][j], ArrayLineOnBag[side][i], MaxSamplingErrorForLOB, MaxSpacingErrorForLOB, out lob))
                            ArrayLineOnBag[side][i] = lob;
                        if (IsFoundNewLine(arrayLob[side][j], side, out lob))
                            LineOnBag.Add(lob, side, ArrayLineOnBag);
                    }
            }
            return ArrayLineOnBag;
        }

        private void UpdateArrayLineOnBag(LineOnBag[][] arrayLob)
        {
            CounterFrames++;

            if (ArrayLineOnBag == null || CounterFrames >= MaxFrames)
                CounterFrames = 0;
            else
                ArrayLineOnBag = ConnectLines(arrayLob);               // Modificate lines

            if (CounterFrames == 0) ArrayLineOnBag = arrayLob;          // First Frame

        }

        /// <summary>
        /// Проверка общей длины стороны и длины определеннной линии. Пока только идея
        /// </summary>
        /// <param name="lobsOld"></param>
        /// <param name="lobsNew"></param>
        /// <param name="side"></param>
        private bool IsFoundNewLine(LineOnBag newLob, int side, out LineOnBag lob)
        {
            lob = newLob;
            bool bCombined = false;
            LocatedLine userPoint;

            foreach (LineOnBag line in ArrayLineOnBag[side])
            {
                LocatedLine.DetermineLocation(newLob, line, out userPoint);
                if (LineOnBag.LineDistanceLess(line, newLob, MaxDistanceForConnect)) bCombined = true;
                else if (userPoint.location_A == Location.Midle || userPoint.location_B == Location.Midle) bCombined = true;
                else if (userPoint.location_A == Location.CloserToA && userPoint.location_B == Location.CloserToB) bCombined = true;
                else if (userPoint.location_A == Location.CloserToB && userPoint.location_B == Location.CloserToA) bCombined = true;
            }

            if (bCombined) return false;
            else return true;

            // double dFreeDist = (double)(lobsOld[0].points.Length - GetDistance(lobsOld[0].A, lobsOld[0].B));    // примерно так 
            //высчитать можно ли продолжить линию
            //Создать точку конца линии после проверки всей длины
        }

        /// <summary>
        ///  Возвращает массив линий готовых к выводу на экран
        /// </summary>
        /// <param name="depthSpeed">массив точек вокруг мешка, первый индексатор отвечает за сторону мешка с которой идет поиск точек</param>
        /// <returns>Возвращает линии проведенные через точки</returns>
        public List<LineOnBag> ModifiedBagLines(Point[][] depthSpeed)
        {
            if (depthSpeed == null) return null;

            List<LineOnBag> list = new List<LineOnBag>();
            List<LineOnBag> tmp;
            LineOnBag[][] arrLob = new LineOnBag[4][];

            foreach (Sides side in Enum.GetValues(typeof(Sides)))
            {
                tmp = GetListLineOnBag(depthSpeed, side);
                arrLob[(int)side] = new LineOnBag[tmp.Count];
                for (int i = 0; i < tmp.Count; i++)
                    arrLob[(int)side][i] = tmp[i];
                tmp.Clear();
            }
            list = LineMerging(arrLob);

            return list;
        }
        
        private bool GetLine(Point[][] pointsArr, Sides side, SpeedInfo infoArr, out LineOnBag lob)
        {
            if (CheckDataForCreateLine(pointsArr[(int)side], infoArr.pCount, infoArr.begIndex, infoArr.endIndex, side, out lob))
                return true;
            else return false;
        }

        private float GetSpeed(Sides side, Point[] arr, int n, int k)
        {
            if (n + k < arr.Length)
            {
                if (side == Sides.Left || side == Sides.Right)
                    return (float)(arr[n + k].X - arr[n].X) / k;
                else
                    return (float)(arr[n + k].Y - arr[n].Y) / k;
            }
            else
                return 999;
        }

        /// <summary>
        /// !!! Возвращает лист для структуры LineOnBag для определенной стороны
        /// </summary>
        /// <param name="arr">DepthSpeed точки скорости перехода</param>
        /// <param name="side">Сторона для которой  формируется лист линий</param>
        /// <returns></returns>
        private List<LineOnBag> GetListLineOnBag(Point[][] arr, Sides side)
        {
            List<LineOnBag> list = new List<LineOnBag>();
            int iLastIndex = 0;

            for (; iLastIndex + MinLengthShortLine < arr[(int)side].Length;)
            {
                int indexLine = -1;
                SpeedInfo[] infoArr = GetSideInfo(arr[(int)side], side, iLastIndex);
                if (FindFirstLine(infoArr, iLastIndex, out indexLine))
                {
                    indexLine = CheckRegion(infoArr, indexLine);
                    LineOnBag line;
                    if (indexLine != -1 && GetLine(arr, side, infoArr[indexLine], out line))
                    {
                        iLastIndex = infoArr[indexLine].endIndex;
                        list.Add(line);
                    }
                    else iLastIndex += 1;
                }
                else iLastIndex += 1;
            }
            return list;
        }

        /// <summary>
        /// Возвращает индекс линии если она максимально близка к условиям выборки
        /// </summary>
        /// <param name="infoArr">массив производных</param>
        /// <param name="indexLine">индекс с которого начинается проверка</param>
        /// <returns>индекс линии</returns>
        private int CheckRegion(SpeedInfo[] infoArr, int indexLine)
        {
            if (indexLine == -1) return -1;

            int aMax = 0;
            int index = indexLine;
            for (int i = indexLine; i < infoArr.Length && i < indexLine + MinLengthShortLine; i++)
                if (infoArr[i].pCount >= MinLengthShortLine 
                    && infoArr[i].apCount > aMax 
                    && infoArr[i].begIndex < infoArr[i].endIndex)
                {
                    aMax = infoArr[i].apCount;
                    index = i;
                }
            return index;
        }

        private SpeedInfo[] GetSideInfo(Point[] points, Sides side, int indBegin = 0)
        {
            SpeedInfo[] arrInfo = new SpeedInfo[points.Length];
            int iCountForError = MinLengthShortLine / 4;    // 3;

            for (int iBase = indBegin; iBase < points.Length - 1; iBase++)
            {
                if (points[iBase].X == 0 && points[iBase].Y == 0) break;

                float iStandard = GetSpeed(side, points, iBase, 1);

                SpeedInfo info = new SpeedInfo();
                info.begIndex = iBase;

                int iCountError = 0;
                for (int K = 1; K + iBase < points.Length; K++)
                {
                    float iSpeed = GetSpeed(side, points, iBase, K);
                    if (iSpeed == 0)
                    {
                        info.pCount++;
                        info.apCount++;
                        info.endIndex = iBase + K;
                        if (iCountError > 0) iCountError--;
                    }
                    else if ((iStandard - MaxErrorOfSpeed) <= iSpeed 
                        && iSpeed <= (iStandard + MaxErrorOfSpeed) 
                        && (Math.Abs(points[K].X - points[iBase].X) <= MaxSamplingError))
                    {
                        info.pCount++;
                        if (iCountError > 0) iCountError--;
                    }
                    else
                    {
                        info.pCount++;
                        iCountError++;
                    }

                    if (iCountError >= iCountForError) break;
                }
                arrInfo[iBase] = info;
            }

            return arrInfo;
        }

        private bool FindFirstLine(SpeedInfo[] infoArr, int index, out int lineInfo)
        {
            lineInfo = -1;
            for (int i = index; i < infoArr.Length; i++)
                if (infoArr[i].pCount >= MinLengthShortLine && infoArr[i].begIndex < infoArr[i].endIndex)
                {
                    lineInfo = i;
                    return true;
                }
            return false;
        }

        /// <summary>
        /// Вовзвращает true если была создана элемент структуры LineOnBug 
        /// </summary>
        /// <param name="points">массив точек, границ мешков</param>
        /// <param name="iPixels">кол-во пикселей от А до Б</param>
        /// <param name="iBegin">индекс в массиве points</param>
        /// <param name="iEnd">индекс в массиве points</param>
        /// <param name="line">возвращаемый параметр</param>
        /// <returns>true - успешное выполнение</returns>
        private bool CheckDataForCreateLine(Point[] points, int iPixels, int iBegin, int iEnd, Sides side, out LineOnBag line)
        {
            line = new LineOnBag();
            line.points = points;
            if (MinLengthShortLine <= iPixels)
            {
                line.A = points[iBegin];
                if (side == Sides.Left || side == Sides.Right)
                    line.B = new Point(points[iBegin].X,
                        points[iEnd >= points.Length ? points.Length - 1 : iEnd].Y);
                else
                    line.B = new Point(points[iEnd >= points.Length ? points.Length - 1 : iEnd].X,
                        points[iBegin].Y);
                line.iCount = iPixels;
                return true;
            }
            else return false;
        }
    }


}
