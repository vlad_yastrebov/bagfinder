﻿namespace BagFinder
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelSize = new System.Windows.Forms.Label();
            this.checkBoxUpdatePB = new System.Windows.Forms.CheckBox();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.bDrawingPanel = new System.Windows.Forms.Button();
            this.bGrid = new System.Windows.Forms.Button();
            this.tb_Border = new System.Windows.Forms.TextBox();
            this.bt_emulation = new System.Windows.Forms.Button();
            this.label_test = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_DelayRead = new System.Windows.Forms.TextBox();
            this.comboBoxMode = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_SizeH = new System.Windows.Forms.TextBox();
            this.tb_SizeW = new System.Windows.Forms.TextBox();
            this.tb_BegY = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_BegX = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.bt_ClearGrid = new System.Windows.Forms.Button();
            this.button_GridPix = new System.Windows.Forms.Button();
            this.button_Graph = new System.Windows.Forms.Button();
            this.labelPixelColor = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panelGrid = new System.Windows.Forms.Panel();
            this.bMenu = new System.Windows.Forms.Button();
            this.panelCells1 = new BagFinder.PanelCells();
            this.chBoxDrawSD = new System.Windows.Forms.CheckBox();
            this.chBoxDrawRecSearch = new System.Windows.Forms.CheckBox();
            this.panelDrawing = new System.Windows.Forms.Panel();
            this.chBoxDrawBags = new System.Windows.Forms.CheckBox();
            this.chBoxDrawLineOnBag = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_MaxFrames = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelMenu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.panelGrid.SuspendLayout();
            this.panelDrawing.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(510, 348);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBox1_MouseMove);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 51);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Y";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(14, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "X";
            // 
            // labelSize
            // 
            this.labelSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSize.AutoSize = true;
            this.labelSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSize.Location = new System.Drawing.Point(238, 84);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(88, 20);
            this.labelSize.TabIndex = 3;
            this.labelSize.Text = "W:***  H:***";
            // 
            // checkBoxUpdatePB
            // 
            this.checkBoxUpdatePB.AutoSize = true;
            this.checkBoxUpdatePB.Checked = true;
            this.checkBoxUpdatePB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUpdatePB.Location = new System.Drawing.Point(3, 3);
            this.checkBoxUpdatePB.Name = "checkBoxUpdatePB";
            this.checkBoxUpdatePB.Size = new System.Drawing.Size(127, 17);
            this.checkBoxUpdatePB.TabIndex = 6;
            this.checkBoxUpdatePB.Text = "Обновление кадров";
            this.checkBoxUpdatePB.UseVisualStyleBackColor = true;
            // 
            // panelMenu
            // 
            this.panelMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelMenu.Controls.Add(this.button1);
            this.panelMenu.Controls.Add(this.bDrawingPanel);
            this.panelMenu.Controls.Add(this.bGrid);
            this.panelMenu.Controls.Add(this.tb_Border);
            this.panelMenu.Controls.Add(this.bt_emulation);
            this.panelMenu.Controls.Add(this.label_test);
            this.panelMenu.Controls.Add(this.label3);
            this.panelMenu.Controls.Add(this.tb_DelayRead);
            this.panelMenu.Controls.Add(this.comboBoxMode);
            this.panelMenu.Controls.Add(this.groupBox1);
            this.panelMenu.Controls.Add(this.checkBoxUpdatePB);
            this.panelMenu.Controls.Add(this.labelSize);
            this.panelMenu.Location = new System.Drawing.Point(528, 12);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(422, 177);
            this.panelMenu.TabIndex = 7;
            // 
            // bDrawingPanel
            // 
            this.bDrawingPanel.Location = new System.Drawing.Point(251, 151);
            this.bDrawingPanel.Name = "bDrawingPanel";
            this.bDrawingPanel.Size = new System.Drawing.Size(75, 23);
            this.bDrawingPanel.TabIndex = 21;
            this.bDrawingPanel.Text = "Рисование";
            this.bDrawingPanel.UseVisualStyleBackColor = true;
            this.bDrawingPanel.Click += new System.EventHandler(this.BDrawingPanel_Click);
            // 
            // bGrid
            // 
            this.bGrid.Location = new System.Drawing.Point(329, 151);
            this.bGrid.Name = "bGrid";
            this.bGrid.Size = new System.Drawing.Size(90, 23);
            this.bGrid.TabIndex = 20;
            this.bGrid.Text = "Меню сетки";
            this.bGrid.UseVisualStyleBackColor = true;
            this.bGrid.Click += new System.EventHandler(this.BGrid_Click);
            // 
            // tb_Border
            // 
            this.tb_Border.Location = new System.Drawing.Point(6, 117);
            this.tb_Border.Name = "tb_Border";
            this.tb_Border.Size = new System.Drawing.Size(100, 20);
            this.tb_Border.TabIndex = 19;
            this.tb_Border.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Tb_Border_KeyUp);
            // 
            // bt_emulation
            // 
            this.bt_emulation.Location = new System.Drawing.Point(145, 20);
            this.bt_emulation.Name = "bt_emulation";
            this.bt_emulation.Size = new System.Drawing.Size(75, 23);
            this.bt_emulation.TabIndex = 15;
            this.bt_emulation.Text = "Emulation";
            this.bt_emulation.UseVisualStyleBackColor = true;
            this.bt_emulation.Click += new System.EventHandler(this.bt_emulation_Click);
            // 
            // label_test
            // 
            this.label_test.AutoSize = true;
            this.label_test.Location = new System.Drawing.Point(13, 101);
            this.label_test.Name = "label_test";
            this.label_test.Size = new System.Drawing.Size(83, 13);
            this.label_test.TabIndex = 14;
            this.label_test.Text = "fBorder(def:0.5f)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 26);
            this.label3.TabIndex = 13;
            this.label3.Text = "Задержка \r\nмежду кадрами";
            // 
            // tb_DelayRead
            // 
            this.tb_DelayRead.Location = new System.Drawing.Point(10, 75);
            this.tb_DelayRead.Name = "tb_DelayRead";
            this.tb_DelayRead.Size = new System.Drawing.Size(50, 20);
            this.tb_DelayRead.TabIndex = 12;
            this.tb_DelayRead.TextChanged += new System.EventHandler(this.Tb_DelayRead_TextChanged);
            // 
            // comboBoxMode
            // 
            this.comboBoxMode.FormattingEnabled = true;
            this.comboBoxMode.Location = new System.Drawing.Point(0, 22);
            this.comboBoxMode.Name = "comboBoxMode";
            this.comboBoxMode.Size = new System.Drawing.Size(121, 21);
            this.comboBoxMode.TabIndex = 11;
            this.comboBoxMode.SelectedValueChanged += new System.EventHandler(this.ComboBoxMode_SelectedValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_SizeH);
            this.groupBox1.Controls.Add(this.tb_SizeW);
            this.groupBox1.Controls.Add(this.tb_BegY);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tb_BegX);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(238, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(181, 78);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Box";
            // 
            // tb_SizeH
            // 
            this.tb_SizeH.Location = new System.Drawing.Point(115, 48);
            this.tb_SizeH.Name = "tb_SizeH";
            this.tb_SizeH.Size = new System.Drawing.Size(52, 20);
            this.tb_SizeH.TabIndex = 3;
            this.tb_SizeH.Text = "70";
            this.tb_SizeH.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tb_SizeH_KeyPress);
            // 
            // tb_SizeW
            // 
            this.tb_SizeW.Location = new System.Drawing.Point(115, 22);
            this.tb_SizeW.Name = "tb_SizeW";
            this.tb_SizeW.Size = new System.Drawing.Size(52, 20);
            this.tb_SizeW.TabIndex = 0;
            this.tb_SizeW.Text = "90";
            this.tb_SizeW.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tb_SizeW_KeyPress);
            // 
            // tb_BegY
            // 
            this.tb_BegY.Location = new System.Drawing.Point(28, 48);
            this.tb_BegY.Name = "tb_BegY";
            this.tb_BegY.Size = new System.Drawing.Size(52, 20);
            this.tb_BegY.TabIndex = 3;
            this.tb_BegY.Text = "25";
            this.tb_BegY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tb_BegY_KeyPress);
            this.tb_BegY.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Tb_BegY_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(94, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "H";
            // 
            // tb_BegX
            // 
            this.tb_BegX.Location = new System.Drawing.Point(28, 22);
            this.tb_BegX.MaxLength = 4;
            this.tb_BegX.Name = "tb_BegX";
            this.tb_BegX.Size = new System.Drawing.Size(52, 20);
            this.tb_BegX.TabIndex = 0;
            this.tb_BegX.Text = "40";
            this.tb_BegX.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tb_BegX_KeyPress);
            this.tb_BegX.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Tb_BegX_KeyUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(94, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "W";
            // 
            // bt_ClearGrid
            // 
            this.bt_ClearGrid.Location = new System.Drawing.Point(120, 32);
            this.bt_ClearGrid.Name = "bt_ClearGrid";
            this.bt_ClearGrid.Size = new System.Drawing.Size(75, 23);
            this.bt_ClearGrid.TabIndex = 18;
            this.bt_ClearGrid.Text = "Clear Grid";
            this.bt_ClearGrid.UseVisualStyleBackColor = true;
            this.bt_ClearGrid.Click += new System.EventHandler(this.bt_ClearGrid_Click);
            // 
            // button_GridPix
            // 
            this.button_GridPix.Location = new System.Drawing.Point(3, 3);
            this.button_GridPix.Name = "button_GridPix";
            this.button_GridPix.Size = new System.Drawing.Size(111, 23);
            this.button_GridPix.TabIndex = 17;
            this.button_GridPix.Text = "Сетка пикселей";
            this.button_GridPix.UseVisualStyleBackColor = true;
            this.button_GridPix.Click += new System.EventHandler(this.Button_GridPix_Click);
            // 
            // button_Graph
            // 
            this.button_Graph.Location = new System.Drawing.Point(120, 3);
            this.button_Graph.Name = "button_Graph";
            this.button_Graph.Size = new System.Drawing.Size(75, 23);
            this.button_Graph.TabIndex = 16;
            this.button_Graph.Text = "График";
            this.button_Graph.UseVisualStyleBackColor = true;
            this.button_Graph.Click += new System.EventHandler(this.Button_Graph_Click);
            // 
            // labelPixelColor
            // 
            this.labelPixelColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelPixelColor.AutoSize = true;
            this.labelPixelColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelPixelColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPixelColor.Location = new System.Drawing.Point(12, 363);
            this.labelPixelColor.Name = "labelPixelColor";
            this.labelPixelColor.Size = new System.Drawing.Size(82, 20);
            this.labelPixelColor.TabIndex = 12;
            this.labelPixelColor.Text = "Pixel Color";
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(6, 67);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(395, 395);
            this.chart1.TabIndex = 14;
            this.chart1.Text = "chart1";
            // 
            // panelGrid
            // 
            this.panelGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelGrid.Controls.Add(this.bMenu);
            this.panelGrid.Controls.Add(this.chart1);
            this.panelGrid.Controls.Add(this.bt_ClearGrid);
            this.panelGrid.Controls.Add(this.panelCells1);
            this.panelGrid.Controls.Add(this.button_Graph);
            this.panelGrid.Controls.Add(this.button_GridPix);
            this.panelGrid.Location = new System.Drawing.Point(528, 12);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(422, 472);
            this.panelGrid.TabIndex = 15;
            this.panelGrid.Visible = false;
            // 
            // bMenu
            // 
            this.bMenu.Location = new System.Drawing.Point(329, 3);
            this.bMenu.Name = "bMenu";
            this.bMenu.Size = new System.Drawing.Size(90, 23);
            this.bMenu.TabIndex = 19;
            this.bMenu.Text = "Главное меню";
            this.bMenu.UseVisualStyleBackColor = true;
            this.bMenu.Click += new System.EventHandler(this.BMenu_Click);
            // 
            // panelCells1
            // 
            this.panelCells1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCells1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelCells1.CountCellsH = 7;
            this.panelCells1.CountCellsV = 7;
            this.panelCells1.Location = new System.Drawing.Point(6, 67);
            this.panelCells1.Name = "panelCells1";
            this.panelCells1.Size = new System.Drawing.Size(395, 395);
            this.panelCells1.TabIndex = 13;
            // 
            // chBoxDrawSD
            // 
            this.chBoxDrawSD.AutoSize = true;
            this.chBoxDrawSD.Checked = true;
            this.chBoxDrawSD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chBoxDrawSD.Location = new System.Drawing.Point(6, 6);
            this.chBoxDrawSD.Name = "chBoxDrawSD";
            this.chBoxDrawSD.Size = new System.Drawing.Size(115, 17);
            this.chBoxDrawSD.TabIndex = 0;
            this.chBoxDrawSD.Text = "Draw Speed depth";
            this.chBoxDrawSD.UseVisualStyleBackColor = true;
            // 
            // chBoxDrawRecSearch
            // 
            this.chBoxDrawRecSearch.AutoSize = true;
            this.chBoxDrawRecSearch.Checked = true;
            this.chBoxDrawRecSearch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chBoxDrawRecSearch.Location = new System.Drawing.Point(6, 29);
            this.chBoxDrawRecSearch.Name = "chBoxDrawRecSearch";
            this.chBoxDrawRecSearch.Size = new System.Drawing.Size(144, 17);
            this.chBoxDrawRecSearch.TabIndex = 1;
            this.chBoxDrawRecSearch.Text = "Draw Reactangle search";
            this.chBoxDrawRecSearch.UseVisualStyleBackColor = true;
            // 
            // panelDrawing
            // 
            this.panelDrawing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelDrawing.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelDrawing.Controls.Add(this.chBoxDrawBags);
            this.panelDrawing.Controls.Add(this.chBoxDrawLineOnBag);
            this.panelDrawing.Controls.Add(this.label4);
            this.panelDrawing.Controls.Add(this.tb_MaxFrames);
            this.panelDrawing.Controls.Add(this.chBoxDrawRecSearch);
            this.panelDrawing.Controls.Add(this.chBoxDrawSD);
            this.panelDrawing.Location = new System.Drawing.Point(528, 195);
            this.panelDrawing.Name = "panelDrawing";
            this.panelDrawing.Size = new System.Drawing.Size(422, 268);
            this.panelDrawing.TabIndex = 20;
            this.panelDrawing.Visible = false;
            // 
            // chBoxDrawBags
            // 
            this.chBoxDrawBags.AutoSize = true;
            this.chBoxDrawBags.Checked = true;
            this.chBoxDrawBags.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chBoxDrawBags.Location = new System.Drawing.Point(6, 75);
            this.chBoxDrawBags.Name = "chBoxDrawBags";
            this.chBoxDrawBags.Size = new System.Drawing.Size(78, 17);
            this.chBoxDrawBags.TabIndex = 5;
            this.chBoxDrawBags.Text = "Draw Bags";
            this.chBoxDrawBags.UseVisualStyleBackColor = true;
            // 
            // chBoxDrawLineOnBag
            // 
            this.chBoxDrawLineOnBag.AutoSize = true;
            this.chBoxDrawLineOnBag.Checked = true;
            this.chBoxDrawLineOnBag.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chBoxDrawLineOnBag.Location = new System.Drawing.Point(6, 52);
            this.chBoxDrawLineOnBag.Name = "chBoxDrawLineOnBag";
            this.chBoxDrawLineOnBag.Size = new System.Drawing.Size(110, 17);
            this.chBoxDrawLineOnBag.TabIndex = 4;
            this.chBoxDrawLineOnBag.Text = "Draw Line on bag";
            this.chBoxDrawLineOnBag.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(304, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Frames";
            // 
            // tb_MaxFrames
            // 
            this.tb_MaxFrames.Location = new System.Drawing.Point(351, 6);
            this.tb_MaxFrames.Name = "tb_MaxFrames";
            this.tb_MaxFrames.Size = new System.Drawing.Size(52, 20);
            this.tb_MaxFrames.TabIndex = 2;
            this.tb_MaxFrames.TextChanged += new System.EventHandler(this.Tb_MaxFrames_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(132, 117);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 22;
            this.button1.Text = "TEST";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 520);
            this.Controls.Add(this.panelDrawing);
            this.Controls.Add(this.labelPixelColor);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panelGrid);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.panelGrid.ResumeLayout(false);
            this.panelDrawing.ResumeLayout(false);
            this.panelDrawing.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.CheckBox checkBoxUpdatePB;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tb_BegX;
        private System.Windows.Forms.TextBox tb_BegY;
        private System.Windows.Forms.TextBox tb_SizeH;
        private System.Windows.Forms.TextBox tb_SizeW;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPixelColor;
        private System.Windows.Forms.ComboBox comboBoxMode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_DelayRead;
        private System.Windows.Forms.Label label_test;
        private PanelCells panelCells1;
        private System.Windows.Forms.Button bt_emulation;
        private System.Windows.Forms.Button button_GridPix;
        private System.Windows.Forms.Button button_Graph;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button bt_ClearGrid;
        private System.Windows.Forms.TextBox tb_Border;
        private System.Windows.Forms.Panel panelGrid;
        private System.Windows.Forms.Button bMenu;
        private System.Windows.Forms.Button bGrid;
        private System.Windows.Forms.Button bDrawingPanel;
        public System.Windows.Forms.CheckBox chBoxDrawRecSearch;
        public System.Windows.Forms.CheckBox chBoxDrawSD;
        private System.Windows.Forms.Panel panelDrawing;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_MaxFrames;
        public System.Windows.Forms.CheckBox chBoxDrawBags;
        public System.Windows.Forms.CheckBox chBoxDrawLineOnBag;
        private System.Windows.Forms.Button button1;
    }
}

