﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagFinder
{
    public struct DisplayElements
    {
        public bool DrawSpeedDepth;
        public bool DrawReactSearch;
        public bool DrawLineOnBag;
        public bool DrawBags;

       
        public DisplayElements(bool bSpeedDepth, bool bDrawReactSearch, bool bLineOnBag, bool bBags)
        {
            DrawSpeedDepth = bSpeedDepth;
            DrawReactSearch = bDrawReactSearch;
            DrawLineOnBag = bLineOnBag;
            DrawBags = bBags;
        }

    }

    public static class DataSetting
    {

        #region Variables

        private static DisplayElements _displayElements = new DisplayElements();

        private static double _dMaxSamplingErrorForLOB = 1d;
        private static double _dMaxSpacingErrorForLOB = 3d;

        private static double _dMaxDistForParallelLines = 2d;

        private static double _dPercentageOfMistakes = 5;

        #endregion

        public static DisplayElements Display
        {
            get { return _displayElements; }
            set { _displayElements = value; }
        }

        public static double MaxSamplingErrorForLOB
        {
            get { return _dMaxSamplingErrorForLOB; }
            set { _dMaxSamplingErrorForLOB = value; }
        }
        public static double MaxSpacingErrorForLOB
        {
            get { return _dMaxSpacingErrorForLOB; }
            set { _dMaxSpacingErrorForLOB = value; }
        }


        public static double DistParallelLines
        {
            get { return _dMaxDistForParallelLines; }
            set { _dMaxDistForParallelLines = value; }
        }

        public static double PercentageOfMistakes
        {
            get { return _dPercentageOfMistakes; }
            set { _dPercentageOfMistakes = value; }
        }
    }
}
