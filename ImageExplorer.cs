﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace BagFinder
{
    static class ImageExplorer
    {
        public static Bitmap NoiseCleaner(Bitmap _bmp, int iCleaner)
        {
            switch (iCleaner)
            {
                case 1:
                    return Cleaner1(_bmp);
                    break;
                case 2:
                    return Cleaner2(_bmp);
                    break;
                default:
                    return Cleaner2(_bmp);
            }
        }
        /// <summary>
        /// Сравнение соседних пикселей по горизонтали, вычисление среднего значения.
        ///  INT I11,I12,I13;
        ///  abs(I11-I13) <=2
        ///  abs((I11+13)/2 - I12) < 2
        ///  I12 = I12 or I12 = abs(I11+13)/2
        /// </summary>
        /// <param name="_bmp"></param>
        /// <returns></returns>
        private static Bitmap Cleaner1(Bitmap _bmp)
        {
            unsafe
            {
                Bitmap bitmapResult = new Bitmap(_bmp);
                //Bitmap bitmapResult = _bmp.Clone(new Rectangle(0, 0, _bmp.Width, _bmp.Height), _bmp.PixelFormat);
                BitmapData bitmapData1 = bitmapResult.LockBits(new Rectangle(0, 0, bitmapResult.Width, bitmapResult.Height), ImageLockMode.ReadWrite, bitmapResult.PixelFormat);
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bitmapResult.PixelFormat) / 8;
                int heightInPixels = bitmapData1.Height;
                int widthInBytes = bitmapData1.Width * bytesPerPixel;
                byte* ptrFirstPixel = (byte*)bitmapData1.Scan0;
                byte val = 0;
                for (int y = 0; y < heightInPixels; y++)
                {
                    byte* currentLineBm1 = ptrFirstPixel + (y * bitmapData1.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        if (x == 0 || x == widthInBytes - bytesPerPixel) continue;
                        int left = x - bytesPerPixel;
                        int right = x + bytesPerPixel;

                        if (Math.Abs(currentLineBm1[left] - currentLineBm1[right]) <= 2)
                        {
                            val = (byte)((currentLineBm1[left] + currentLineBm1[right]) / 2);
                            currentLineBm1[x] = val;
                            currentLineBm1[x + 1] = val;
                            currentLineBm1[x + 2] = val;
                        }
                        //else continue;
                    }
                }
                bitmapResult.UnlockBits(bitmapData1);
                return bitmapResult;
            }
        }
        private static Bitmap Cleaner2(Bitmap _bmp)
        {
            unsafe
            {
                Bitmap bitmapResult = new Bitmap(_bmp);
                //Bitmap bitmapResult = _bmp.Clone(new Rectangle(0, 0, _bmp.Width, _bmp.Height), _bmp.PixelFormat);
                BitmapData bitmapData1 = bitmapResult.LockBits(new Rectangle(0, 0, bitmapResult.Width, bitmapResult.Height), ImageLockMode.ReadWrite, bitmapResult.PixelFormat);
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bitmapResult.PixelFormat) / 8;
                int heightInPixels = bitmapData1.Height;
                int widthInBytes = bitmapData1.Width * bytesPerPixel;
                byte* ptrFirstPixel = (byte*)bitmapData1.Scan0;
                byte val = 0;
                for (int y = 1; y < heightInPixels - 1; y++)
                {
                    byte* currentLineBm0 = ptrFirstPixel + ((y - 1) * bitmapData1.Stride);
                    byte* currentLineBm1 = ptrFirstPixel + (y * bitmapData1.Stride);
                    byte* currentLineBm2 = ptrFirstPixel + ((y + 1) * bitmapData1.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)        //x - middle pixel
                    {
                        if (x == 0 || x == widthInBytes - bytesPerPixel) continue;
                        int left = x - bytesPerPixel;
                        int right = x + bytesPerPixel;

                        int limit = BitLogic.DifferenceForMidle(3, currentLineBm1[x], currentLineBm1[left], currentLineBm1[right],
                            currentLineBm0[x], currentLineBm0[left], currentLineBm0[right],
                            currentLineBm2[x], currentLineBm2[left], currentLineBm2[right]);
                        if (limit > 4)
                        {
                            val = BitLogic.Max(currentLineBm1[left], currentLineBm1[right]);
                            currentLineBm1[x] = val;
                            currentLineBm1[x + 1] = val;
                            currentLineBm1[x + 2] = val;
                        }
                        continue;
                        //if (Math.Abs(currentLineBm1[left] - currentLineBm1[right]) <= 2)
                        //{
                        //    val = (byte)((currentLineBm1[left] + currentLineBm1[right]) / 2);
                        //    currentLineBm1[x] = val;
                        //    currentLineBm1[x + 1] = val;
                        //    currentLineBm1[x + 2] = val;
                        //}
                        ////else continue;
                    }
                }
                bitmapResult.UnlockBits(bitmapData1);
                return bitmapResult;
            }
        }
        public static Bitmap FastInverseColor(Bitmap bm1)
        {
            unsafe
            {
                Bitmap bitmapResult = bm1.Clone(new Rectangle(0, 0, bm1.Width, bm1.Height), bm1.PixelFormat);
                BitmapData bitmapData1 = bitmapResult.LockBits(new Rectangle(0, 0, bitmapResult.Width, bitmapResult.Height), ImageLockMode.ReadWrite, bitmapResult.PixelFormat);
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bm1.PixelFormat) / 8;
                int heightInPixels = bitmapData1.Height;
                int widthInBytes = bitmapData1.Width * bytesPerPixel;
                byte* ptrFirstPixel = (byte*)bitmapData1.Scan0;

                for (int y = 0; y < heightInPixels; y++)
                {
                    byte* currentLineBm1 = ptrFirstPixel + (y * bitmapData1.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int oldBlue = 255 - currentLineBm1[x];
                        int oldGreen = 255 - currentLineBm1[x + 1];
                        int oldRed = 255 - currentLineBm1[x + 2];

                        // calculate new pixel value
                        currentLineBm1[x] = (byte)oldBlue;
                        currentLineBm1[x + 1] = (byte)oldGreen;
                        currentLineBm1[x + 2] = (byte)oldRed;
                    }
                }
                bitmapResult.UnlockBits(bitmapData1);
                return bitmapResult;
            }
        }
    }


    static class BitLogic
    {
        public static bool AbsDifference(byte _arg1, byte _arg2, byte _border)
        {
            bool result = Math.Abs(_arg1 - _arg2) < _border ? true : false;
            return result;
        }
        public static int DifferenceForMidle(int _border, params byte[] values)
        {   // values.Length = 2 - min  // values[0] = midle point
            if (values.Length < 2)
            {
                Console.WriteLine("Error");
                return -1;
            }

            int result = 0;     // beyond the limit
            for (int i = 1; i < values.Length; i++)
                result = Math.Abs(values[i] - values[0]) < _border ? result : result + 1;


            return result;
        }
        public static byte Min(params byte[] values)
        {
            if (values.Length == 0)
            {
                Console.WriteLine("Error");
                return 255;
            }
            byte min = 255;
            for (int i = 0; i < values.Length; i++)
                if (values[i] < min) min = values[i];


            return min;
        }
        public static byte Max(params byte[] values)
        {
            if (values.Length == 0)
            {
                Console.WriteLine("Error");
                return 0;
            }
            byte max = 0;
            for (int i = 0; i < values.Length; i++)
                if (values[i] > max) max = values[i];


            return max;
        }

    }


    static class HandleControl
    {

    }
}
