﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace BagFinder
{

    /// <summary>
    /// Перечисления сторон изображения для обращения в массиве
    /// </summary>
    public enum Sides
    {
        Left = 0,
        Up = 1,
        Right = 2,
        Down = 3
    }


    /// <summary>
    /// Линия/сторона мешка(полная/неполная)
    /// </summary>
    public class LineOnBag
    {
        public Sides axis;
        public int iCount;          // Количество точек // При значении 0, линия исключается из массива
        public int iAxisPoint;      // Координаты X или Y при условии параллельности линии границе кадра.
        public Point[] points;
        public Point A;         // begin line
        public bool bCornerA;
        public Point B;         // end line
        public bool bCornerB;

        //public static bool operator ==(LineOnBag lob1, LineOnBag lob2)
        //{
        //    return lob1.Equals(lob2);
        //}

        //public static bool operator !=(LineOnBag lob1, LineOnBag lob2)
        //{
        //    return !lob1.Equals(lob2);
        //}

        public static void Add(LineOnBag lob, int side, LineOnBag[][] arr)
        {

            if (arr[side].Length == 0 && lob != null)
            {
                arr[side] = new LineOnBag[1];
                arr[side][0] = lob;
            }
            else if (arr[side].Length != 0)
            {
                LineOnBag[] arrTemp = new LineOnBag[arr[side].Length + 1];
                int i = 0;
                foreach (LineOnBag line in arr[side])
                {
                    arrTemp[i] = line;
                    i++;
                }
                arrTemp[arrTemp.Length - 1] = lob;
                arr[side] = arrTemp;
            }
        }

        /// <summary>
        /// Возвращает расстояние между двумя точками
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>расстояние</returns>
        private static double GetDistance(Point a, Point b)
        {
            return Geometry.GetDistance(a, b);
        }

        /// <summary>
        /// объединяет крайние точки двух линий в случае если ближайшие их точки удовлетворяют условию MaxDistanceForConnect
        /// </summary>
        /// <param name="l1">линия должна быть удалена за пределами функции</param>
        /// <param name="l2">возвращаемя лини с измененной точкой и iCount</param>
        /// <returns>Линия сложенная из двух</returns>
        public static LineOnBag CombineLines(LineOnBag l1, LineOnBag l2, double MaxDistanceForConnect)
        {
            if (GetDistance(l2.A, l1.A) < MaxDistanceForConnect) { l2.A = l1.B; l2.iCount = (int)GetDistance(l2.A, l2.B); }
            else
            if (GetDistance(l2.A, l1.B) < MaxDistanceForConnect) { l2.A = l1.A; l2.iCount = (int)GetDistance(l2.A, l2.B); }
            else
            if (GetDistance(l2.B, l1.A) < MaxDistanceForConnect) { l2.B = l1.B; l2.iCount = (int)GetDistance(l2.A, l2.B); }
            else
            if (GetDistance(l2.B, l1.B) < MaxDistanceForConnect) { l2.B = l1.A; l2.iCount = (int)GetDistance(l2.A, l2.B); }

            return l2;
        }

        /// <summary>
        /// Возвращает минимальное расстояние и положение относительно отрезка.
        /// </summary>
        /// <param name="lobNew"></param>
        /// <param name="lobOld"></param>
        /// <param name="loc">Положение</param>
        /// <returns>Возвращает минимальное расстояние</returns>
        private static double GetMinDistance(LineOnBag lobNew, LineOnBag lobOld, out Location loc)
        {
            loc = Location.Undefined;
            double min = Geometry.GetDistance(lobOld.A, lobOld.B);
            if (Geometry.GetDistance(lobOld.A, lobNew.A) < min) { min = Geometry.GetDistance(lobOld.A, lobNew.A); loc = Location.CloserToA; }
            if (Geometry.GetDistance(lobOld.A, lobNew.B) < min) { min = Geometry.GetDistance(lobOld.A, lobNew.B); loc = Location.CloserToB; }
            if (Geometry.GetDistance(lobOld.B, lobNew.A) < min) { min = Geometry.GetDistance(lobOld.B, lobNew.A); loc = Location.CloserToA; }
            if (Geometry.GetDistance(lobOld.B, lobNew.B) < min) { min = Geometry.GetDistance(lobOld.B, lobNew.B); loc = Location.CloserToB; }
            return min;
        }

        /// <summary>
        /// Объединяет два отрезка
        /// </summary>
        /// <param name="lobNew"></param>
        /// <param name="lobOld"></param>
        /// <param name="lobResult">Объединенный отрезок</param>
        /// <returns>Истина, если получен объединенный отрезок</returns>
        public static bool IsSuccessfullyCombined(LineOnBag lobNew, LineOnBag lobOld, double dMaxSamplingError, double dMaxSpacingError, out LineOnBag lobResult)
        {
            Tuple<Point, Point> oldLine = Geometry.ResizeLine(lobOld.A, lobOld.B);
            LocatedLine userPoint;
            lobResult = lobOld;

            if (Geometry.GetDistance(oldLine.Item1, oldLine.Item2, lobNew.A) < dMaxSamplingError &&
                Geometry.GetDistance(oldLine.Item1, oldLine.Item2, lobNew.B) < dMaxSamplingError)  // проверка расстояния до прямой 
            {
                LocatedLine.DetermineLocation(lobNew, lobResult, out userPoint);

                // точка A на отрезке
                if (userPoint.location_A == Location.Midle)
                    if (userPoint.location_B == Location.CloserToA && Geometry.GetDistance(lobResult.A, lobResult.B) < Geometry.GetDistance(userPoint.point_B, lobResult.B))       // точка B дальше отрезка
                    { lobResult.A = userPoint.point_B; return true; }
                    else if (userPoint.location_B == Location.CloserToB && Geometry.GetDistance(lobResult.A, lobResult.B) < Geometry.GetDistance(lobResult.A, userPoint.point_B))  // точка B дальше отрезка
                    { lobResult.B = userPoint.point_B; return true; }

                // точка B на отрезке
                if (userPoint.location_B == Location.Midle)
                    if (userPoint.location_A == Location.CloserToA && Geometry.GetDistance(lobResult.A, lobResult.B) < Geometry.GetDistance(userPoint.point_A, lobResult.A))       // точка A дальше отрезка
                    { lobResult.A = userPoint.point_A; return true; }
                    else if (userPoint.location_A == Location.CloserToB && Geometry.GetDistance(lobResult.A, lobResult.B) < Geometry.GetDistance(lobResult.A, userPoint.point_A))  // точка A дальше отрезка
                    { lobResult.B = userPoint.point_A; return true; }

                // точка A на A, точка B на B
                if (userPoint.location_A == Location.CloserToA && userPoint.location_B == Location.CloserToB && Geometry.GetDistance(lobResult.A, lobResult.B) < Geometry.GetDistance(userPoint.point_A, userPoint.point_B))
                {
                    lobResult.A = userPoint.point_A;
                    lobResult.B = userPoint.point_B;
                    return true;
                }

                // точка A на B, точка B на A
                if (userPoint.location_A == Location.CloserToB && userPoint.location_B == Location.CloserToA && Geometry.GetDistance(lobResult.A, lobResult.B) < Geometry.GetDistance(userPoint.point_B, userPoint.point_A))
                {
                    lobResult.A = userPoint.point_B;
                    lobResult.B = userPoint.point_A;
                    return true;
                }

                // точка A на A, точка B на A, имеется пустой промежуток между отрезками
                if (userPoint.location_A == Location.CloserToA && userPoint.location_B == Location.CloserToA)
                {
                    Location loc;
                    if (GetMinDistance(lobNew, lobResult, out loc) <= dMaxSpacingError)
                        if (loc == Location.CloserToA)
                            lobResult.A = userPoint.point_B;
                        else if (loc == Location.CloserToB)
                            lobResult.A = userPoint.point_A;
                    return true;
                }

                // точка A на B, точка B на B, имеется пустой промежуток между отрезками
                if (userPoint.location_A == Location.CloserToB && userPoint.location_B == Location.CloserToB)
                {
                    Location loc;
                    if (GetMinDistance(lobNew, lobResult, out loc) <= dMaxSpacingError)
                        if (loc == Location.CloserToA && Geometry.GetDistance(lobResult.A, lobResult.B) < Geometry.GetDistance(lobResult.A, userPoint.point_B))
                        { lobResult.B = userPoint.point_B; return true; }
                        else if (loc == Location.CloserToB && Geometry.GetDistance(lobResult.A, lobResult.B) < Geometry.GetDistance(lobResult.A, userPoint.point_A))
                        { lobResult.B = userPoint.point_A; return true; }
                }
            }

            return false;
        }

        /// <summary>
        /// Очищает массив от пустых линий в которых iCount == 0
        /// </summary>
        /// <param name="arrayLob"></param>
        public static LineOnBag[][] CleaningArrayOfLines(LineOnBag[][] arrayLob)
        {
            foreach (Sides side in Enum.GetValues(typeof(Sides)))
            {
                // Перестановка всех не пустых линий в начало.
                for (int i = 0; i < arrayLob[(int)side].Length - 1; i++)
                {
                    bool rearranged = false;

                    if (arrayLob[(int)side][i].iCount == 0) arrayLob[(int)side][i] = arrayLob[(int)side][i + 1];

                    if (rearranged && arrayLob[(int)side][i + 1].iCount != 0) i = 0;
                }

                // Определение размера нового массива не меньше 1ого
                int length = arrayLob[(int)side].Length;
                for (int i = arrayLob[(int)side].Length - 1; i >= 0; i--)
                    if (arrayLob[(int)side][i].iCount == 0 && length > 1) length--;

                // создание нового массива без пустых линий
                if (length != arrayLob[(int)side].Length)
                {
                    LineOnBag[] lobArr = new LineOnBag[length];
                    for (int i = 0; i < lobArr.Length; i++)
                    {
                        lobArr[i] = arrayLob[(int)side][i];
                    }
                    arrayLob[(int)side] = lobArr;
                }
                // Непрервные прямые по всей длине изображения
                //for (int i = 0; i < arrayLob[(int)side].Length; i++)
                //{
                //    Tuple<PointF,PointF> temp = ResizeLine(arrayLob[(int)side][i].A, arrayLob[(int)side][i].B);
                //    arrayLob[(int)side][i].A.X = (int)temp.Item1.X;
                //    arrayLob[(int)side][i].A.Y = (int)temp.Item1.Y;
                //    arrayLob[(int)side][i].B.X = (int)temp.Item2.X;
                //    arrayLob[(int)side][i].B.Y = (int)temp.Item2.Y;
                //}
            }
            return arrayLob;
        }

        /// <summary>
        /// Сравнение новой линии со старой. Если любая пара точек на расстоянии меньше заданного возвращает true(за 30 кадров то не будет изменяться основная информация о линиях...)
        /// </summary>
        /// <param name="lob1">ллиния 1</param>
        /// <param name="lob2">ллиния 2</param>
        /// <param name="maxDist">Максимальная разрешимая дистанция между линиями</param>
        /// <returns> True - линии близко друг к другу</returns>
        public static bool LineDistanceLess(LineOnBag lob1, LineOnBag lob2, float maxDist)
        {
            if (Geometry.GetDistance(lob1.A, lob2.A) < maxDist) return true;
            if (Geometry.GetDistance(lob1.A, lob2.B) < maxDist) return true;

            if (Geometry.GetDistance(lob1.B, lob2.A) < maxDist) return true;
            if (Geometry.GetDistance(lob1.B, lob2.B) < maxDist) return true;

            return false;
        }

    }

}
