﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace BagFinder
{

    /// <summary>
    /// класс задумывался для калибровки камеры, если камера расположена слегка под углом
    /// задумка: берутся глубины двух точек на одинаковом отдалении от центра и вычисляется коэфициент на которой 
    /// </summary>
    public static class Geometry
    {
        /// <summary>
        /// Возвращает координату середины отрезка между двумя точками
        /// </summary>
        /// <param name="A">Начало</param>
        /// <param name="B">Конец</param>
        /// <returns>координата середины отрезка</returns>
        public static Point GetMidlePoint(Point A, Point B)
        {
            return new Point((A.X + B.X) / 2, (A.Y + B.Y) / 2);
        }

        /// <summary>
        /// Возвращает угол между отрезками AB BC 
        /// </summary>
        /// <param name="B"></param>
        /// <param name="A"></param>
        /// <param name="C"></param>
        /// <returns>Угол в радианах</returns>
        public static double GetAngle(Point A, Point B, Point C)
        {
            Point AB = new Point((A.X - B.X), (A.Y - B.Y));
            Point AC = new Point((C.X - B.X), (C.Y - B.Y));

            double proiz = AB.X * AC.X + AB.Y * AC.Y;
            double modAB = Math.Sqrt(AB.X * AB.X + AB.Y * AB.Y);
            double modAC = Math.Sqrt(AC.X * AC.X + AC.Y * AC.Y);

            double cos = proiz / (modAB * modAC);
            double gradus = Math.Acos(cos) * 180 / Math.PI;

            return gradus;
        }

        /// <summary>
        /// Возвращает угол отрезка
        /// </summary>
        /// <param name="A">Начало отрезка</param>
        /// <param name="B">Конец отрезка</param>
        /// <returns>Угловой коэффициент</returns>
        public static double GetAngle(Point A, Point B)
        {
            double distY = B.Y - A.Y;
            double distX = B.X - A.X;
            return Math.Atan(distY / distX);
        }

        /// <summary>
        /// Возвращает угол поворота мешка относительно оси координат
        /// </summary>
        /// <param name="A">Координата хвоста мешка</param>
        /// <param name="B">Координата хвоста мешка 2</param>
        /// <returns>Угол в радианах</returns> 
        public static double GetAngleBag(Point A, Point B)
        {
            Point ordA = new Point(0, 0);
            Point ordB = new Point(B.X - A.X, B.Y - A.Y);
            Point ordC = new Point(0, 50);
            if (ordB.X == 0) return 0;
            else if (ordB.Y == 0) return 90;
            return GetAngle(ordB, ordA, ordC);
        }

        /// <summary>
        /// Возвращает расстояние между двумя точками
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>расстояние</returns>
        public static double GetDistance(Point a, Point b)
        {
            return Math.Sqrt((double)(b.X - a.X) * (double)(b.X - a.X) + (double)(b.Y - a.Y) * (double)(b.Y - a.Y));
        }

        /// <summary>
        /// Возвращает длину вектора
        /// </summary>
        /// <param name="vector">Вектор</param>
        /// <returns>Длина вектора</returns>
        public static double GetDistance(Point vector)
        {
            return Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y);
        }

        public static PointF GetUnitVector(Point vector)
        {
            float length = (float)GetDistance(vector);

            return new PointF((float)vector.X / length, (float)vector.Y / length);
        }

        /// <summary>
        /// Вовзращает расстояние между отрезком и точкой
        /// </summary>
        /// <returns></returns>
        public static double GetDistance(Point A, Point B, Point p0)
        {
            if (PointInTheLine(A, B, p0)) return 0;

            double AB = GetDistance(A, B);
            double Ap = GetDistance(A, p0);
            double pB = GetDistance(p0, B);
            double p = 0.5 * (AB + Ap + pB);
            double H = (2 * Math.Sqrt(p * (p - AB) * (p - Ap) * (p - pB))) / AB;

            return H;
        }

        /// <summary>
        /// Проверка положения точки относительно отрезка
        /// </summary>
        /// <param name="A">начало отрезка</param>
        /// <param name="B">конец отрезка</param>
        /// <param name="p0">точка на отрезке</param>
        /// <returns>Подтверждение того что точка на отрезке</returns>
        public static bool PointInTheLine(Point A, Point B, Point p0)
        {
            if (A.X == B.X || A.Y == B.Y) return false;        // предотвращает деление на ноль

            double res1 = (p0.X - A.X) / (B.X - A.X);
            double res2 = (p0.Y - A.Y) / (B.Y - A.Y);

            if (res1 == res2)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Увеличение отрезка по всей области размером 176:144
        /// </summary>
        /// <param name="start">Начальная точка отрезка</param>
        /// <param name="end">Конечная точка отрезка</param>
        /// <param name="length">Длина, на которую нужно увеличить отрезок</param>
        /// <returns>Возвращает две точки — новые координаты отрезка</returns>
        public static Tuple<Point, Point> ResizeLine(Point start, Point end)
        {
            Size size = new Size(176, 144);
            float length;
            //Считаем вектор
            var v = new PointF(end.X - start.X, end.Y - start.Y);
            //Длина вектора
            var l = (float)Math.Sqrt(v.X * v.X + v.Y * v.Y);
            //Нормирование
            v = new PointF(v.X / l, v.Y / l);

            //Новые координаты отрезка 
            length = start.X / v.X;
            length = start.Y - v.Y * length > 0 ? length : start.Y / v.Y;
            var newStart = new Point((int)(start.X - v.X * length), (int)(start.Y - v.Y * length));
            length = (size.Width - end.X) / v.X;
            length = end.Y + v.Y * length < size.Height ? length : (size.Height - end.Y) / v.Y;
            var newEnd = new Point((int)(end.X + v.X * length), (int)(end.Y + v.Y * length));

            return new Tuple<Point, Point>(newStart, newEnd);
        }

        /// <summary>
        /// Возвращает точку пересечения отрезков AB CD
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="C"></param>
        /// <param name="D"></param>
        /// <returns></returns>
        public static Point Intersection(Point A, Point B, Point C, Point D)
        {
            double xo = A.X, yo = A.Y;
            double p = B.X - A.X, q = B.Y - A.Y;

            double x1 = C.X, y1 = C.Y;
            double p1 = D.X - C.X, q1 = D.Y - C.Y;

            double x = (xo * q * p1 - x1 * q1 * p - yo * p * p1 + y1 * p * p1) /
                (q * p1 - q1 * p);
            double y = (yo * p * q1 - y1 * p1 * q - xo * q * q1 + x1 * q * q1) /
                (p * q1 - p1 * q);

            return new Point((int)x, (int)y);
        }

        /// <summary>
        /// Отрезок AB и CD пересекатся
        /// </summary>
        /// <param name="A">Начало AB</param>
        /// <param name="B">Конец AB</param>
        /// <param name="C">Начало CD</param>
        /// <param name="D">Конец CD</param>
        /// <returns>Истина - Отрезки пересекаются</returns>
        public static bool IsIntersection(Point A, Point B, Point C, Point D)
        {
            if (LinesIsParallel(A, B, C, D)) return false;      // линии параллельный следовательно не могут пересекаться

            Point p0 = Intersection(A, B, C, D);
            if (PointInTheLine(A, B, p0) && PointInTheLine(C, D, p0))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Проверка параллельности отрезков AB и CD 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="C"></param>
        /// <param name="D"></param>
        /// <returns>Истина - отрезки параллельны</returns>
        public static bool LinesIsParallel(Point A, Point B, Point C, Point D)
        {
            if (A.X == B.X && C.X == D.X) return true;      // линии параллельны оси X
            if (A.Y == B.Y && C.Y == D.Y) return true;      // линии параллельны оси X

            double angleAB = GetAngle(A, B);
            double angleCD = GetAngle(C, D);

            if (angleAB == angleCD) return true;

            return false;
        }

        /// <summary>
        /// Возвращает точку самую отдаленную от первой.
        /// </summary>
        /// <param name="pBegin">Относительно данной точки сравниваются остальные</param>
        /// <param name="A1">точка 1</param>
        /// <param name="A2">точка 2</param>
        /// <returns>Возвращает 1 или 2 точку</returns>
        public static Point GetDistantPoint(Point pBegin, Point A1, Point A2)
        {
            if (GetDistance(pBegin, A1) > GetDistance(pBegin, A2)) return A1;
            else return A2;
        }
    }
}
