﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace BagFinder
{

    /// <summary>
    /// показывает расположение точки относительно отрезка
    /// </summary>
    enum Location
    {
        CloserToA = 1,      // A точка на lobOld
        CloserToB = 2,      // B точка на lobOld
        Midle = 3,          // между A и B на lobOld
        Superimposed = 4,   // точка накладывается
        Undefined = 5       // не определенно
    }
    class LocatedLine
    {

        #region Variables
        public Point point_A;
        public Location location_A;
        public Point point_B;
        public Location location_B;
        #endregion



        #region Construct
        public LocatedLine(Point A, Point B)
        {
            point_A = A;
            point_B = B;
            location_A = Location.Undefined;
            location_B = Location.Undefined;
        }
        #endregion



        #region Methods
        /// <summary>
        /// Инициализирует направлаения(Location)
        /// </summary>
        /// <param name="lobFirst">Новый отрезок, точки которого определяют в каком направлении лежат</param>
        /// <param name="lobSecond">Старый отрезок</param>
        /// <param name="userPoint">Возвращаемая структура</param>
        public static void DetermineLocation(LineOnBag lobFirst, LineOnBag lobSecond, out LocatedLine userPoint)
        {
            userPoint = new LocatedLine(lobFirst.A, lobFirst.B);
            double distOld = Geometry.GetDistance(lobSecond.A, lobSecond.B);


            if (Geometry.GetDistance(lobSecond.A, lobFirst.A) <= distOld && Geometry.GetDistance(lobSecond.B, lobFirst.A) <= distOld)   // точка A лежит внутри отрезка
                userPoint.location_A = Location.Midle;
            else if (Geometry.GetDistance(lobSecond.A, lobFirst.A) < Geometry.GetDistance(lobSecond.B, lobFirst.A))
                userPoint.location_A = Location.CloserToA;
            else if (Geometry.GetDistance(lobSecond.A, lobFirst.A) > Geometry.GetDistance(lobSecond.B, lobFirst.A))
                userPoint.location_A = Location.CloserToB;
            else
                userPoint.location_A = Location.Undefined;

            if (Geometry.GetDistance(lobSecond.A, lobFirst.B) < distOld && Geometry.GetDistance(lobSecond.B, lobFirst.B) < distOld)     // точка B лежит внутри отрезка
                userPoint.location_B = Location.Midle;
            else if (Geometry.GetDistance(lobSecond.A, lobFirst.B) < Geometry.GetDistance(lobSecond.B, lobFirst.B))
                userPoint.location_B = Location.CloserToA;
            else if (Geometry.GetDistance(lobSecond.A, lobFirst.B) > Geometry.GetDistance(lobSecond.B, lobFirst.B))
                userPoint.location_B = Location.CloserToB;
            else
                userPoint.location_A = Location.Undefined;
        }
        #endregion

    }
}
